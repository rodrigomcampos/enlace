﻿using Repositorio.Aplicacao.Extension;
using System;
using System.ComponentModel.DataAnnotations;

namespace Repositorio.Aplicacao.Validate
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CpfAttribute : ValidationAttribute
    {
        /// <summary>
        /// Indica se conteúdos vazios devem ser validados
        /// </summary>
        public bool ValidateEmpty { get; set; }

        /// <summary>
        /// Verifica se o conteúdo da propriedade representa um CPF
        /// </summary>
        /// <param name="value">Valor da propriedade para ser validado</param>
        /// <returns>true caso o conteúdo represente um CPF, caso contrário, false</returns>
        public override bool IsValid(object value)
        {
            string cpf = value as string;

            if (!cpf.IsEmpty() || this.ValidateEmpty)
                return Validadores.IsCpf(cpf);

            return true;
        }
    }
}