﻿using Repositorio.Aplicacao.Extension;
using System;
using System.ComponentModel.DataAnnotations;

namespace Repositorio.Aplicacao.Validate
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class EmailAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var email = value as string;

            if (email.IsEmpty())
                return false;

            if (!Validadores.IsEmail(email))
                return false;

            return true;
        }
    }
}