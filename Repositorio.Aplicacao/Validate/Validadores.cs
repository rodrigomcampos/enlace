﻿using System.Text.RegularExpressions;

namespace Repositorio.Aplicacao.Validate
{
    public static class Validadores
    {       
        public static bool IsEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;
            else
                return Regex.IsMatch(email, @"^(\w+(\.\w+){0,}@\w+\.+\w+(\.\w{2}){0,1})$");
        }
        
        public static bool IsCnpj(string cnpj)
        {
            if (string.IsNullOrWhiteSpace(cnpj))
                return false;

            if (!Regex.IsMatch(cnpj, @"^(\d{14}|\d{2}(\.\d{3}){2}\/\d{4}-\d{2})$"))
                return false;

            string numerosCnpj = Regex.Replace(cnpj, @"[^\d]", "");
            string primeirosDoze = numerosCnpj.Substring(0, 12);
            int primeiroDigito = GetDigitoVerificadorCnpj(primeirosDoze);
            int segundoDigito = GetDigitoVerificadorCnpj(string.Concat(primeirosDoze, primeiroDigito.ToString()));

            return numerosCnpj.Substring(12) == string.Concat(primeiroDigito.ToString(), segundoDigito.ToString());
        }
        
        public static bool IsCpf(string cpf)
        {
            if (string.IsNullOrWhiteSpace(cpf))
                return false;

            if (!Regex.IsMatch(cpf, @"^(\d{11}|\d{3}(\.\d{3}){2}-\d{2})$"))
                return false;

            string numerosCpf = Regex.Replace(cpf, @"[^\d]", "");
            string primeirosNove = numerosCpf.Substring(0, 9);
            int primeiroDigito = GetDigitoVerificadorCpf(primeirosNove);
            int segundoDigito = GetDigitoVerificadorCpf(string.Concat(primeirosNove, primeiroDigito.ToString()));

            return numerosCpf.Substring(9) == string.Concat(primeiroDigito.ToString(), segundoDigito.ToString());
        }

        private static int GetDigitoVerificadorCnpj(string digitos)
        {
            int soma = 0;

            soma += SomaDigitos(digitos, 0, digitos.Length - 8, digitos.Length - 7);
            soma += SomaDigitos(digitos, digitos.Length - 8, digitos.Length, 9);

            return GetDigitoVerificador(soma);
        }

        private static int GetDigitoVerificador(int soma)
        {
            int resto = soma % 11;

            if (resto < 2)
                return 0;
            else
                return 11 - resto;
        }

        private static int SomaDigitos(string digitos, int inicio, int termino, int peso)
        {
            int soma = 0;
            int digito;

            for (var i = inicio; i < termino; i++)
            {
                digito = int.Parse(digitos[i].ToString());
                soma += digito * peso--;
            }

            return soma;
        }

        private static int GetDigitoVerificadorCpf(string digitos)
        {
            int soma = SomaDigitos(digitos, 0, digitos.Length, digitos.Length + 1);

            return GetDigitoVerificador(soma);
        }
    }
}