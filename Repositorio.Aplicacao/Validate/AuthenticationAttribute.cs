﻿using Repositorio.Aplicacao.Extension;
using Repositorio.Aplicacao.Util;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Repositorio.Aplicacao.Validate
{
    [AttributeUsage(AttributeTargets.All)]
    public sealed class AuthenticationAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            base.HandleUnauthorizedRequest(actionContext);

            actionContext.Response = new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.Unauthorized,
                RequestMessage = actionContext.Request
            };
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            HttpRequestMessage request = actionContext.Request;
            AuthenticationHeaderValue authorization = request.Headers.Authorization;

            if (authorization == null)
            {
                // No authentication was attempted (for this authentication method).
                // Do not set either Principal (which would indicate success) or ErrorResult (indicating an error).
                return false;
            }

            if (authorization.Scheme != "Bearer")
            {
                // No authentication was attempted (for this authentication method).
                // Do not set either Principal (which would indicate success) or ErrorResult (indicating an error).
                return false;
            }

            if (authorization.Parameter.IsEmpty())
            {
                // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                return false;
            }

            var principal = Autenticacao.GetPrincipalFromToken(authorization.Parameter);
            Thread.CurrentPrincipal = principal;

            return (principal != null);
        }
    }
}