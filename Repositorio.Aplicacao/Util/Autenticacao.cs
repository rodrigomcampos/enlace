﻿using JWT;
using Newtonsoft.Json;
using Repositorio.Aplicacao.Comum;
using Repositorio.Aplicacao.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security.Claims;
using System.Threading;

namespace Repositorio.Aplicacao.Util
{
    internal static class Autenticacao
    {
        private static string USERID_KEY = "userId";
        private static string TYPEUSER_KEY = "typeUser";
        private static string apikey = "3nl4c3_p1r4c1c4b4";
        private static DateTime unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static ClaimsPrincipal GetPrincipal()
        {
            return (ClaimsPrincipal)Thread.CurrentPrincipal;
        }

        public static string GeraToken(Usuario usuario)
        {
            var expiry = Math.Round((DateTime.UtcNow.AddHours(24) - unixEpoch).TotalSeconds);
            var issuedAt = Math.Round((DateTime.UtcNow - unixEpoch).TotalSeconds);

            var payload = new Dictionary<string, object>
            {
                {USERID_KEY, usuario.IdUsuario},
                {TYPEUSER_KEY, usuario.Tipo},
                {"sub", usuario.Login},
                {"iat", issuedAt},
                {"exp", expiry}
            };

            return JsonWebToken.Encode(payload, apikey, JwtHashAlgorithm.HS256);
        }

        [SuppressMessage("Microsoft.Design", "CA1031", Justification = "O processo não pode parar")]
        public static ClaimsPrincipal GetPrincipalFromToken(string token)
        {
            try
            {
                Dictionary<string, object> tokenDecodificado = DecodificaToken(token);

                if (TokenExpirado(tokenDecodificado))
                    return null;

                return CriaPrincipal(tokenDecodificado);
            }
            catch (Exception ex)
            {
                Log.Erro(ex, $"Erro ao obter o usuário do token {token}");
                return null;
            }
        }

        private static ClaimsPrincipal CriaPrincipal(Dictionary<string, object> tokenDecodificado)
        {
            Claim id = new Claim(ClaimTypes.SerialNumber, tokenDecodificado[USERID_KEY].ToString());
            Claim typeUser = new Claim(ClaimTypes.GroupSid, tokenDecodificado[TYPEUSER_KEY].ToString());

            IList<Claim> claims = new List<Claim>() { id, typeUser };
            ClaimsIdentity identity = new ClaimsIdentity(claims, "Token");
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            return principal;
        }

        public static Dictionary<string, object> DecodificaToken(string token)
        {
            string tokenJson = JsonWebToken.Decode(token, apikey);
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(tokenJson);
        }

        public static bool TokenExpirado(Dictionary<string, object> tokenDecodificado)
        {
            object exp;
            if (tokenDecodificado != null && tokenDecodificado.TryGetValue("exp", out exp))
            {
                var validTo = FromUnixTime(long.Parse(exp.ToString()));
                if (DateTime.Compare(validTo, DateTime.UtcNow) >= 0)
                    return false;
            }

            return true;
        }

        private static DateTime FromUnixTime(long unixTime)
        {
            return unixEpoch.AddSeconds(unixTime);
        }
    }
}