$(document).ready(function () {

    if (sessionStorage.length == 0)
        window.location.replace("index.html");

    function configuraAjax() {
        $.ajaxSetup({
            async: true,
            cache: false,
            dataType: "json",
            contentType: 'application/json; charset=UTF-8'
        });

        $.ajaxPrefilter(function (options) {
            var token = sessionStorage.getItem('token');
            var headers = options.headers || {};

            if (token)
                headers.Authorization = 'Bearer ' + token;

            options.headers = headers;
        });
    }

    configuraAjax();

    $("#cursos_link").click(util.carregarPagina("cursos.html"));

    $("#eventos_link").click(util.carregarPagina("eventos.html"));

    $("#biblioteca_link").click(util.carregarPagina("biblioteca.html"));

    $("#cursos_link").click();

});

$(document).ajaxError(function (e, xhr, settings) {
    if (xhr.status == 401) {
        aviso.exibeMensagemUnico('Sua sessão expirou. Por favor, realize o login novamente');
        setTimeout(function () {
            window.location.replace("index.html");
        }, 10000);
    }
});

$.ajaxSetup({
    async: true,
    cache: false,
    dataType: 'json',
    global: true,
    beforeSend: function (jqXHR, settings) {
        if ($(".loading").is(':hidden') && settings.type == "POST")
            $(".loading").fadeIn();
    },
    complete: function () {
        if ($(".loading").is(':visible'))
            $(".loading").fadeOut();
    }
});