var cadastro = (function () {

    function animacaoEdicao(div, animate) {
        $(animate).animate({
            scrollTop: 0
        }, "slow");
    };

    function visualizarEdicao(div) {
        animacaoEdicao(div, "html, body");
    };

    function dadosValidos(classe) {
        var valido = true;
        $(".espaco_campo input, .espaco_campo select").filter("[required]").each(function () {
            if ($(this).val() == '' && $(this).val() !== 0) {
                valido = false;
            }
        });

        return valido;
    };

    function cadastroSucesso() {
        aviso.exibe("Dados cadastrados com sucesso.");
        $("input[type='text']:not(.auto_val), input[type='password'], textarea").val("");
        $(".select2_html:not(.auto_val):not(#config_part_figura), .select2_alt:not(.auto_val)").val("-1").trigger("change");
        $("input[type='text']:not(.auto_val), input[type='password'], textarea").val("").removeClass("campo_erro");
        $(".select2-selection").removeClass("campo_erro");
        $(".campo_extra").remove();
    };

    function cadastroFalha() {
        aviso.exibe("Os campos com * ao lado são obrigatórios.");
        $(".espaco_campo input:required, .select_obrigatorio .select2-selection").addClass("campo_erro");
    };

    function finalizarEdicao(edt) {
        $(".select2_html:not(.auto_val):not(#config_part_figura), .select2_alt:not(.auto_val)").val("-1").trigger("change");
        $(".criar_espaco, .cancelar_espaco").css('display', 'inline-block');
        $(".cancelar_edicao, .editar_espaco").hide();
        $("input[type='text']:not(.auto_val), input[type='password'], textarea").val("").removeClass("campo_erro");
        $(".select_obrigatorio .select2-selection").removeClass("campo_erro");
        $(".campo_extra").remove();
        if (edt === 1)
            aviso.exibe("Dados editados com sucesso.");
    };

    return {
        visualizarEdicao: visualizarEdicao,
        cadastroSucesso: cadastroSucesso,
        cadastroFalha: cadastroFalha,
        finalizarEdicao: finalizarEdicao,
        dadosValidos: dadosValidos,
    };
})(jQuery);