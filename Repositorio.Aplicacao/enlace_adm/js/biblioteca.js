var idArquivo;

$(document).ready(function () {

    opcoes.carregaTiposBiblioteca("#tipo_arquivo").then(function () {
        util.aplicarSelect2(".select2_html");
    });

    util.configuraDatatables();

    tabelaBiblioteca();

    function dadosBiblioteca() {
        return {
            IdArquivo: idArquivo,
            Tipo: $("#tipo_arquivo").val(),
            Titulo: $("#titulo_arquivo").val(),
            Autor: $("#autor_arquivo").val(),
            Arquivo: $("#link_arquivo").val(),
            Ativo: ($("#ativo_arquivo").val() === "true")
        }
    }

    $("#criar_arquivo").click(function () {

        var dados = dadosBiblioteca();

        if (!cadastro.dadosValidos()) {
            cadastro.cadastroFalha();
        } else {
            $.post("../biblioteca/cadastrar", JSON.stringify(dados), "json").done(function (r) {
                $("#tabela_biblioteca").DataTable().ajax.reload(function () {
                    $.when(cadastro.cadastroSucesso()).then(function () {
                        $("#ativo_arquivo").val("true").change();
                    });
                });
            }).fail(function (r) {
                aviso.exibeMensagens(r.responseJSON);
            });
        }
    });

    $("#cancelar_arquivo").click(function () {
        $(".editar_espaco, .ativo_espaco").hide(function () {
            $(".criar_espaco").css("display", "inline-block");
            $.when(cadastro.finalizarEdicao()).then(function () {
                $("#ativo_arquivo").val("true").change();
            });
        });
    });

    function camposEditados() {

        var dados = dadosBiblioteca();

        if (!cadastro.dadosValidos()) {
            cadastro.cadastroFalha();
        } else {
            util.exibirLoading();
            $.post("../biblioteca/editar", JSON.stringify(dados), "json")
                .done(function (r) {
                    $("#tabela_biblioteca").DataTable().ajax.reload(function () {
                        $.when(cadastro.finalizarEdicao(1)).then(function () {
                            $("#ativo_arquivo").val("true").change();
                        });
                    });
                }).fail(function (r) {
                    aviso.exibeMensagens(r.responseJSON);
                }).always(function () {
                    util.esconderLoading();
                });
        }

    }

    function editarArquivo(id) {
        return function (e) {
            cadastro.visualizarEdicao();
            carregaDadosEdicao(id).then(function () {
                $("#editar_arquivo").off("click").click(camposEditados);
            });
        };
    };

    function carregaDadosEdicao(id) {

        var q = $.Deferred();

        util.exibirLoading();

        idArquivo = id;

        $.get("../biblioteca/arquivo/" + id).done(function (r) {

                preencheCamposEdicao(r);

                util.esconderLoading();

                q.resolve();
            })
            .fail(function (r) {
                q.reject();
            });

        return q.promise();

    }

    function preencheCamposEdicao(r) {

        $(".criar_espaco").hide(function () {
            $(".editar_espaco, .ativo_espaco").css("display", "inline-block");
        });

        $("#tipo_arquivo").val(r[0].Tipo).change();
        $("#titulo_arquivo").val(r[0].Titulo);
        $("#autor_arquivo").val(r[0].Autor);
        $("#link_arquivo").val(r[0].Arquivo);
        $("#ativo_arquivo").val(r[0].Ativo.toString());

    }


    function tabelaBiblioteca() {

        var tabela_cursos = $("#tabela_biblioteca").DataTable({
            ajax: {
                url: "../biblioteca/todosArquivos",
                dataSrc: ""
            },
            columns: [{
                    title: "Data",
                    data: "Data",
                    render: util.renderData
                },
                {
                    title: "ID",
                    data: "IdArquivo"
                },
                {
                    title: "Tipo do Arquivo",
                    data: "Tipo"
                },
                {
                    title: "Título",
                    data: "Titulo"
                },
                {
                    title: "Autor",
                    data: "Autor"
                },
                {
                    title: "Link",
                    data: "Arquivo"
                },
                {
                    title: "Ativo",
                    data: "Ativo",
                    render: function (data, type) {
                        if (data == true)
                            return "Sim";
                        else
                            return "Não";
                    }
                },
                {
                    title: "Editar",
                    render: function (data, type) {
                        if (type == "display")
                            return '<img class="img_dt_tables img_click editar_dado" src="images/editar.svg" alt="Editar" />'
                        else
                            return null;
                    }
                }
            ],
            createdRow: function (row, data) {
                $(".editar_dado", row).click(editarArquivo(data.IdArquivo));
            }
        });

    };


});