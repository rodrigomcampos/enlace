$(document).ready(function () {

    var rota = '../evento/cadastrar';

    util.dateTimePicker("#data_evento");

    util.aplicarSelect2(".select2_html");

    util.configuraDatatables();

    tabelaEventos();

    $("#imagem_evento").change(function () {
        $(".img_selecionado").hide();
        $("input[type='file'").attr("required", "required");
    });

    $("#pdf_evento").change(function () {
        $(".pdf_selecionado").hide();
        $("input[type='file'").attr("required", "required");
    });

    function limparDados() {
        $(".editar_espaco, .ativo_espaco").hide(function () {
            $(".criar_espaco").css("display", "inline-block");
            $("input[type='text'], input[type='file']").val("");
            $(".pre_selecionado").hide();
            $("input[type='file'").attr("required", "required");
            rota = '../evento/cadastrar';
        });
    }

    $("#cancelar_evento").click(function () {
        limparDados();
    });

    $("form#form_evento").submit(function (e) {

        util.exibirLoading();

        e.preventDefault();

        var formData = new FormData();
        var pdf = $('#pdf_evento')[0].files;
        var imagem = $('#imagem_evento')[0].files;

        formData.append("PDF", pdf[0]);
        formData.append("Imagem", imagem[0]);
        formData.append('Nome', $("#titulo_evento").val());
        formData.append('TipoEvento', $("#tipo_evento").val());
        formData.append('Formulario', $("#form_link_evento").val());
        formData.append('Data', $("#data_evento").val());
        formData.append('Horario', $("#horario_evento").val());
        formData.append('Local', $("#local_evento").val());
        formData.append('Ativo', $("#ativo_evento").val() === "true");

        $.ajax({
            url: rota,
            dataType: false,
            processData: false,
            contentType: false,
            data: formData,
            type: 'POST'
        }).done(function (r) {
            $("#tabela_eventos").DataTable().ajax.reload();
            limparDados();
            aviso.exibe(r.responseJSON);
        }).fail(function (r) {
            aviso.exibeMensagens(r.responseJSON);
        }).always(function (r) {
            util.esconderLoading();
        });
    });

    function tabelaEventos() {

        var tabela_cursos = $("#tabela_eventos").DataTable({
            ajax: {
                url: "../evento",
                dataSrc: ""
            },
            columns: [{
                    title: "Data",
                    data: "DataEvento",
                    render: util.renderData
                },
                {
                    title: "Título",
                    data: "NomeEvento"
                },
                {
                    title: "Editar",
                    render: function (data, type) {
                        if (type == "display")
                            return '<img class="img_dt_tables editar_evento img_click" src="images/editar.svg" alt="Editar" />'
                        else
                            return null;
                    }
                }
            ],
            createdRow: function (row, data) {
                $(".editar_evento", row).click(function () {

                    util.exibirLoading();

                    $("input[type='file'").removeAttr("required");

                    $.get("../evento/selecionar/" + data.IdEvento, function (r) {

                        var img = /[^/]*$/.exec(r.UrlImagem)[0];
                        var doc = /[^/]*$/.exec(r.UrlDocumento)[0];

                        $("#titulo_evento").val(r.NomeEvento);
                        $("#tipo_evento").val(r.TipoEvento);
                        $("#form_link_evento").val(r.LinkFormulario);
                        $("#data_evento").datetimepicker('setOptions', {
                            value: (moment(r.DataEvento, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY'))
                        });
                        $("#horario_evento").val(r.Horario);
                        $("#local_evento").val(r.Local);
                        $("#caminho_imagem").html(img);
                        $("#caminho_pdf").html(doc);

                        util.esconderLoading();

                    });

                    $(".criar_espaco").hide(function () {
                        $(".editar_espaco, .ativo_espaco").css("display", "inline-block");
                    });

                    $(".pre_selecionado").show();

                    rota = "../evento/editar/" + data.IdEvento;

                });
            }
        });

    };


});