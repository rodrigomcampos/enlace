var opcoes = (function ($) {

    function carregaEstados(elem) {
        return util.getOptionsSelect({
            url: "Estado",
            elemento: $(elem),
            codigo: "IdEstado",
            nome: "Nome"
        });
    }

    function carregaCidadesDoEstado(elem, idEstado) {
        return util.getOptionsSelect({
            url: "Cidade/" + idEstado,
            elemento: $(elem),
            codigo: "Ibge",
            nome: "Nome"
        });
    }

    function carregaDocumentos(elem) {
        return util.getOptionsSelect({
            url: "../api/opcoes/documentosNecessarios",
            elemento: $(elem),
            codigo: "Valor",
            nome: "Texto"
        });
    }

    function carregaTiposBiblioteca(elem) {
        return util.getOptionsSelect({
            url: "../api/opcoes/tiposBiblioteca",
            elemento: $(elem),
            codigo: "Valor",
            nome: "Texto"
        });
    }

    return {
        carregaEstados: carregaEstados,
        carregaCidadesDoEstado: carregaCidadesDoEstado,
        carregaDocumentos: carregaDocumentos,
        carregaTiposBiblioteca: carregaTiposBiblioteca
    }

})(jQuery);