$(document).ready(function () {

    var idCurso;

    $('textarea').froalaEditor({
        toolbarButtons: ['undo', 'redo', 'bold', 'color'],
        quickInsertButtons: ['hr'],
        language: 'pt_br'
    });

    opcoes.carregaDocumentos("#docs_curso").then(function () {
        util.aplicarSelect2(".select2_html");
    });

    $("#cancelar_curso").click(function () {
        $(".form_cursos").slideUp(function () {
            $("input[type='text']").val("");
            $(".espaco_campo select, input[type='file']").val("").change();
        });
    });

    util.dateTimePicker(".data_inicio_curso");

    function appendLista(lista, campo) {

        const q = $.Deferred();
        const idLinha = $(lista + " li").length;

        $(lista).append("<li id='linha_" + idLinha + "' class='campo_extra'>" + campo + "</li>")

        q.resolve();

        return q.promise();

    }

    function novaLinha(lista, local) {

        const q = $.Deferred();

        const campo = '<div class="campo_extra turma_valores_' + local + '">' +
            '<div class="espaco_campo">' +
            '<label>*Data Início:</label>' +
            '<input class="data_inicio_curso" type="text" placeholder="Selecione a data de início" required />' +
            '</div>' +
            '<div class="espaco_campo">' +
            '<label>Horário:</label>' +
            '<input type="text" class="horario_curso" placeholder="Informe o horário do curso" required />' +
            '</div>' +
            '<div class="espaco_campo">' +
            '<label>*Dia:</label>' +
            '<input type="text" class="dia_curso" placeholder="Informe o dia da semana" required />' +
            '</div>' +
            '<img src="images/remover.svg" alt="remover" class="remover" />' +
            '</div>';

        appendLista(lista, campo).then(function () {
            util.dateTimePicker(".data_inicio_curso");
            q.resolve();
        });

        $(".remover").click(function () {
            $(this).parent().remove();
        });

        return q.promise();

    }

    $("#add_turma_piracicaba").click(function () {
        novaLinha("#lista_turma_piracicaba", "piracicaba");
    });

    $("#add_turma_campinas").click(function () {
        novaLinha("#lista_turma_campinas", "campinas");
    });

    util.configuraDatatables();

    tabelaCursos();

    function editarTurma(r, lista, index, local) {

        const unidades = r.Unidades[index];
        const turmas = unidades.Turmas;

        if (turmas != null)
            $.each(turmas, function (i) {

                let turmaIndice = turmas[i];

                novaLinha(lista, local).then(function () {
                    $("#linha_" + i + " .data_inicio_curso").datetimepicker('setOptions', {
                        value: (moment(turmaIndice.DataInicio, 'YYYY-MM-DD').format('DD/MM/YYYY'))
                    });
                    $("#linha_" + i + " .dia_curso").val(turmaIndice.DiaSemana);
                    $("#linha_" + i + " .horario_curso").val(turmaIndice.Horario);
                });

            });

    }

    function dadosCurso() {

        var jsonCampinas = null;
        var jsonPiracicaba = null;

        var unidades = new Array();
        var documentosNecessarios = new Array();

        if ($("#val_matricula_curso_piracicaba").val() != null) {
            jsonPiracicaba = {
                Unidade: 3538709,
                Dados: {
                    ValorMatricula: parseFloat($("#val_matricula_curso_piracicaba").val()),
                    QuantidadeParcelas: $("#parcelas_curso_piracicaba").val(),
                    ValorParcelas: parseFloat($("#valor_parcelas_curso_piracicaba").val()),
                    Duracao: $("#duracao_curso_piracicaba").val(),
                    CargaHoraria: $("#carga_horaria_curso_piracicaba").val(),
                    EnderecoRealizacao: $("#endereco_curso_piracicaba").val(),
                    Ativo: ($("#ativo_curso_piracicaba").val() === "true"),
                    Turmas: $(".turma_valores_piracicaba").map(function () {
                        return {
                            DiaSemana: $(".dia_curso", this).val(),
                            DataInicio: $(".data_inicio_curso", this).datetimepicker("getValue"),
                            Horario: $(".horario_curso", this).val(),
                        }
                    }).toArray()
                }
            }

            unidades.push(jsonPiracicaba);
        }

        if ($("#val_matricula_curso_campinas").val() != "") {
            jsonCampinas = {
                Unidade: 3509502,
                Dados: {
                    ValorMatricula: parseFloat($("#val_matricula_curso_campinas").val()),
                    QuantidadeParcelas: $("#parcelas_curso_campinas").val(),
                    ValorParcelas: parseFloat($("#valor_parcelas_curso_campinas").val()),
                    Duracao: $("#duracao_curso_campinas").val(),
                    CargaHoraria: $("#carga_horaria_curso_campinas").val(),
                    EnderecoRealizacao: $("#endereco_curso_campinas").val(),
                    Ativo: ($("#ativo_curso_campinas").val() === "true"),
                    Turmas: $(".turma_valores_campinas").map(function () {
                        return {
                            DiaSemana: $(".dia_curso", this).val(),
                            DataInicio: $(".data_inicio_curso", this).datetimepicker("getValue"),
                            Horario: $(".horario_curso", this).val(),
                        }
                    }).toArray()
                }
            };

            unidades.push(jsonCampinas);
        }

        if ($("#docs_curso").val()[0] != "")
            documentosNecessarios = $("#docs_curso").val();

        return {
            IdCurso: idCurso,
            Nome: $("#titulo_curso").val(),
            IdsDocumentosNecessarios: documentosNecessarios,
            Sobre: $("#sobre_curso").froalaEditor('html.get'),
            Informacoes: $("#info_curso").froalaEditor('html.get'),
            Destinado: $("#destinado_curso").froalaEditor('html.get'),
            Ativo: ($("#ativo_curso").val() === "true"),
            Unidades: unidades
        }
    }

    function camposEditados() {

        const dados = dadosCurso();

        if (!cadastro.dadosValidos())
            cadastro.cadastroFalha();
        else
            util.exibirLoading().then(function () {
                $.post("../curso/editar", JSON.stringify(dados), "json").done(function (r) {
                    $("#tabela_cursos").DataTable().ajax.reload(function () {
                        $(".form_cursos").slideUp(function () {
                            cadastro.finalizarEdicao(1);
                        });
                    });
                }).fail(function (r) {
                    aviso.exibeMensagens(r.responseJSON);
                }).always(function () {
                    util.esconderLoading();
                });
            });

    }

    $("#editar_curso").off("click").click(camposEditados);

    function preencherDados(cidade, r) {
        $("#val_matricula_curso_" + cidade).val(r.ValorMatricula);
        $("#parcelas_curso_" + cidade).val(r.QuantidadeParcelas);
        $("#valor_parcelas_curso_" + cidade).val(r.ValorParcelas);
        $("#duracao_curso_" + cidade).val(r.Duracao);
        $("#carga_horaria_curso_" + cidade).val(r.CargaHoraria);
        $("#endereco_curso_" + cidade).val(r.EnderecoRealizacao);
    }

    function tabelaCursos() {

        var tabela_cursos = $("#tabela_cursos").DataTable({
            ajax: {
                url: "../curso",
                dataSrc: ""
            },
            columns: [
                {
                    title: "Data",
                    data: "DataCadastro",
                    render: util.renderData
                },
                {
                    title: "Título",
                    data: "Nome"
                },
                {
                    title: "Editar",
                    render: function (data, type) {
                        if (type == "display")
                            return '<img class="editar_curso img_dt_tables img_click" src="images/editar.svg" alt="Editar" />'
                        else
                            return null;
                    }
                }
            ],
            createdRow: function (row, data) {
                $(".editar_curso", row).off("click").click(function () {

                    util.exibirLoading();

                    idCurso = data.IdCurso;

                    $(".pre_selecionado").show();

                    $(".form_cursos").slideDown();

                    $.get("../curso/" + data.IdCurso + "/dadosAdm").done(function (r) {

                        let dadosPiracicaba;
                        let dadosCampinas;

                        $("input[type='text']").val("");
                        $(".campo_extra").remove();
                        $("#titulo_curso").val(r.Nome);
                        $("#docs_curso").val(r.DocumentosNecessarios).change();
                        $('#sobre_curso').froalaEditor('html.set', r.Sobre);
                        $('#info_curso').froalaEditor('html.set', r.Informacoes);
                        $('#destinado_curso').froalaEditor('html.set', r.Destinado);

                        if (r.Unidades.length == 1) {

                            dadosPiracicaba = r.Unidades[0];

                            preencherDados("piracicaba", dadosPiracicaba);
                            editarTurma(r, "#lista_turma_piracicaba", 0, "piracicaba");

                        } else {

                            dadosPiracicaba = r.Unidades[0];
                            dadosCampinas = r.Unidades[1];

                            preencherDados("piracicaba", dadosPiracicaba);
                            preencherDados("campinas", dadosCampinas);
                            editarTurma(r, "#lista_turma_piracicaba", 0, "piracicaba");
                            editarTurma(r, "#lista_turma_campinas", 1, "campinas");

                        }


                    }).always(function () {
                        util.esconderLoading();
                    });
                });
            }
        });
    };
});