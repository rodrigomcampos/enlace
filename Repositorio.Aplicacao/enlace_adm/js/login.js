(function ($) {

    sessionStorage.clear();

    var loading = 1;

    $(document).ready(function () {
        $("#usuario").focus();
        $("#entrar").click(acessarPainel);
        $("#usuario, #senha").keypress(function (e) {
            if (e.keyCode === 13)
                return acessarPainel();
        });
    });

    function acessarPainel() {
        if ($("#usuario").val().length != 0 && $("#senha").val().length != 0) {
            ajaxEntrar()
                .done(function (ret) {
                    sessionStorage.setItem("token", ret.Token);
                    sessionStorage.setItem("login", ret.Login);
                    sessionStorage.setItem("nomeUsuario", ret.Nome);
                    sessionStorage.setItem("tipoUsuario", ret.TipoUsuario);
                    window.location.replace("home.html");
                })
                .fail(function (ret) {
                    exibirMensagem(ret.responseJSON);
                    $("#entrar").html("Entrar");
                    $("#entrar").prop('disabled', false);
                });
        } else {
            exibirMensagem("Os campos abaixo são obrigatórios.")
        }
    }

    function exibirMensagem(msg) {
        $(".aviso p").html(msg);
        $(".aviso").slideDown().delay(5000).slideUp();
    }

    function exibeErroGenerico() {
        exibirMensagem('Não foi possível completar sua ação, tente novamente.');
        $("#entrar").html("Entrar");
        $("#entrar").prop('disabled', false);
    }

    function carregandoProcesso(botao) {
        return function () {
            if (loading == 1) {
                $(botao).html("<img src='images/loading.svg' class='loading'>");
                $(botao).prop('disabled', true);
            }
        };
    }

    function ajaxEntrar() {
        return $.ajax({
            cache: false,
            url: '../login',
            type: 'POST',
            beforeSend: carregandoProcesso("#entrar"),
            data: {
                Login: $('#usuario').val(),
                Senha: $('#senha').val()
            }
        });
    }

})(jQuery);