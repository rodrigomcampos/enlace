﻿using Repositorio.Aplicacao.Models.Response;
using System.Web.Http;

namespace Repositorio.Aplicacao.Controllers
{
    [RoutePrefix("Estado")]
    public class EstadoController : ApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(EstadoResponse.Estados);
        }
    }
}
