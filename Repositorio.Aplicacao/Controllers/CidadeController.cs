﻿using Repositorio.Aplicacao.Models.Response;
using System.Web.Http;

namespace Repositorio.Aplicacao.Controllers
{
    [RoutePrefix("Cidade")]
    public class CidadeController : ApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("{idEstado:int}")]
        public IHttpActionResult Get(int idEstado)
        {
            return Ok(CidadeResponse.Cidades(idEstado));
        }
    }
}
