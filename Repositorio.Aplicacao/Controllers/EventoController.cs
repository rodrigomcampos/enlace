﻿using Repositorio.Aplicacao.Models;
using Repositorio.Aplicacao.Models.Response;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Repositorio.Aplicacao.Controllers
{
    [RoutePrefix("evento")]
    public class EventoController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(TodosEventosAtivosResponseModel.Get());
        }

        [HttpGet]
        [Route("selecionar/{idEvento:int}")]
        public IHttpActionResult GetEvento(int idEvento)
        {
            return Ok(SelecionarEventoResponseModel.Get(idEvento));
        }

        [HttpPost]
        [Route("cadastrar")]
        public IHttpActionResult Cadastrar()
        {
            if (!Request.Content.IsMimeMultipartContent())
                return Content(HttpStatusCode.UnsupportedMediaType, "Requisição inválida");

            if (!HttpContext.Current.Request.Files.AllKeys.Any())
                return Content(HttpStatusCode.BadRequest, "Nenhum arquivo identificado");

            new GerenciarEvento(HttpContext.Current.Request).CadastrarEvento();
            return Ok("Evento cadastrado com sucesso");
        }

        [HttpPost]
        [Route("editar/{idEvento:int}")]
        public IHttpActionResult Editar(int idEvento)
        {
            if(idEvento == 0)
                return Content(HttpStatusCode.NotFound, "Id do evento inválido");

            if (!Request.Content.IsMimeMultipartContent())
                return Content(HttpStatusCode.UnsupportedMediaType, "Requisição inválida");

            new GerenciarEvento(HttpContext.Current.Request).Editar(idEvento);
            return Ok("Evento editado com sucesso");
        }
    }
}
