﻿using Repositorio.Aplicacao.Comum;
using Repositorio.Aplicacao.Models;
using Repositorio.Aplicacao.Models.Request;
using Repositorio.Aplicacao.Models.Response;
using Repositorio.Aplicacao.Util;
using Repositorio.DAL.Repositorios;
using System.Net;
using System.Web.Http;

namespace Repositorio.Aplicacao.Controllers
{
    [RoutePrefix("login")]
    public class LoginController : ApiController
    {
        [Route("")]
        [AllowAnonymous]
        public IHttpActionResult Post(LoginRequestModel login)
        {
            var usuario = new TB_UsuarioRepositorio().RealizarLogin(login.Login, login.Senha);

            if(usuario == null)
            {
                Log.Erro($"Usuario não identificado | Login: {login.Login} | Senha: {login.Senha}");
                return Content(HttpStatusCode.NotFound, "Usuário não identificado");
            }

            return Ok(new LoginResponse
            {
                Token = Autenticacao.GeraToken(Usuario.Converter(usuario)),
                Nome = usuario.Nome,
                Login = usuario.Login,
                TipoUsuario = usuario.TipoUsuario
            });
        }
    }
}
