﻿using Repositorio.Aplicacao.Exceptions;
using Repositorio.Aplicacao.Models;
using Repositorio.Aplicacao.Models.Request;
using Repositorio.Aplicacao.Validate;
using Repositorio.DAL.Repositorios;
using System.Linq;
using System.Web.Http;

namespace Repositorio.Aplicacao.Controllers
{
    [RoutePrefix("biblioteca")]
    public class BibliotecaController : ApiController
    {
        [HttpGet]
        [Route("todosArquivos")]
        public IHttpActionResult GetArquivos()
        {
            return Ok(new TB_BibliotecaRepositorio().GetArquivos());
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("arquivos")]
        public IHttpActionResult GetArquivosClient()
        {
            var arquivos = new TB_BibliotecaRepositorio().GetArquivosClient();
            return Ok(arquivos);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("livros")]
        public IHttpActionResult GetLivros()
        {
            return Ok(new TB_BibliotecaRepositorio().GetArquivosClient(1));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("artigos")]
        public IHttpActionResult GetArtigos()
        {
            return Ok(new TB_BibliotecaRepositorio().GetArquivosClient(2));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("videos")]
        public IHttpActionResult GetVideos()
        {
            return Ok(new TB_BibliotecaRepositorio().GetArquivosClient(3));
        }

        [HttpGet]
        [Route("arquivo/{idArquivo:int}")]
        public IHttpActionResult GetArquivo(int idArquivo)
        {
            return Ok(new TB_BibliotecaRepositorio()
                .GetWhere(t => t.IdBiblioteca == idArquivo)
                .Select(t => new
                {
                    IdArquivo = t.IdBiblioteca,
                    Titulo = t.Titulo,
                    Autor = t.Autor,
                    Arquivo = t.Arquivo,
                    Ativo = t.Ativo,
                    Tipo = t.CodigoTipo
                })
                .ToList());
        }

        [HttpPost]
        [Route("cadastrar")]
        public IHttpActionResult CadastrarItens([FromBody] CadastrarItemBibliotecaRequestModel item)
        {
            if (item.ValidarSeJaExiste())
                throw new ValidacaoException("Arquivo já cadastrado na base de dados");

            item.Cadastrar();
            return Ok("Cadastro realizado com sucesso");
        }

        [HttpPost]
        [Route("editar")]
        public IHttpActionResult EditarArquivo([FromBody] EditarItemRequestModel editar)
        {
            editar.Editar();
            return Ok("Arquivo editado com sucesso");
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("solicitar")]
        public IHttpActionResult SolicitarArquivos([FromBody] string email)
        {
            var validarEmail = Validadores.IsEmail(email);

            if (validarEmail == false)
                return Content(System.Net.HttpStatusCode.BadRequest, "E-mail inválido");

            new ArquivoBiblioteca().EnviarEmail(email);

            return Ok("Solicitação enviada com sucesso.");
        }
    }
}
