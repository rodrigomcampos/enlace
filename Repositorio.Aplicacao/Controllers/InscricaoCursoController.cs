﻿using Repositorio.Aplicacao.Models;
using Repositorio.Aplicacao.Models.Request;
using System.Web.Http;

namespace Repositorio.Aplicacao.Controllers
{
    [RoutePrefix("Cursos")]
    public class InscricaoCursoController : ApiController
    {
        [HttpPost]
        [AllowAnonymous]
        [Route("inscrever")]
        public IHttpActionResult InscreverCurso([FromBody]InscricaoCursoRequest request)
        {
            var inscricao = new InscricaoCurso(request);
            inscricao.AdicionarNoBanco();
            inscricao.EnviarPorEmail();
            return Ok("Inscricao enviada com sucesso, em breve entraremos em contato!");
        }
    }
}
