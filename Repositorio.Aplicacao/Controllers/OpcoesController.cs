﻿using Repositorio.Aplicacao.Models.Response;
using System.Web.Http;

namespace Repositorio.Aplicacao.Controllers
{
    [RoutePrefix("api/opcoes")]
    public class OpcoesController : ApiController
    {
        [HttpGet]
        [Route("documentosNecessarios")]
        public IHttpActionResult GetDocumentosNecessarios()
        {
            return Ok(OpcoesResponse.GetDocumentosNecessarios());
        }

        [HttpGet]
        [Route("tiposBiblioteca")]
        public IHttpActionResult GetTiposBiblioteca()
        {
            return Ok(OpcoesResponse.GetTipoBiblioteca());
        }
    }
}
