﻿using Repositorio.Aplicacao.Models;
using Repositorio.Aplicacao.Models.Request;
using Repositorio.Aplicacao.Models.Response;
using Repositorio.DAL.Repositorios;
using System.Web.Http;

namespace Repositorio.Aplicacao.Controllers
{
    [RoutePrefix("curso")]
    public class CursoController : ApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(new TB_CursoRepositorio().GetCursos());
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("{codigoCurso:int}/dados")]
        public IHttpActionResult GetCurso(int codigoCurso)
        {
            return Ok(GetCursoResponseModel.GetCurso(codigoCurso));
        }
        
        [HttpGet]
        [Route("{idCurso:int}/dadosAdm")]
        public IHttpActionResult GetCursoAdm(int idCurso)
        {
            return Ok(new TB_CursoRepositorio().GetCursoAdm(idCurso));
        }

        [HttpPost]
        [Route("editar")]
        public IHttpActionResult EditarCurso([FromBody] EditarCursoRequestModel editar)
        {
            var editarCurso = new EditarCurso(editar);
            editarCurso.Editar();
            return Ok("Curso editado com sucesso");
        }
    }
}
