﻿using Repositorio.Aplicacao.Models;
using Repositorio.Aplicacao.Models.Request;
using System.Web.Http;

namespace Repositorio.Aplicacao.Controllers
{
    [RoutePrefix("Contato")]
    [AllowAnonymous]
    public class ContatoController : ApiController
    {
        [Route("Enviar")]
        public IHttpActionResult Enviar([FromBody] ContatoRequestModel contato)
        {
            Email.Enviar(contato.Assunto, new LayoutEmail().CriarHtml(contato));
            return Ok("Mensagem enviada com sucesso");
        }
    }
}
