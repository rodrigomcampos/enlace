﻿using System;
using System.Web.Http;

namespace Repositorio.Aplicacao
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //Comum.Log.RegistraTxt($"Iniciou a aplicação em {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}", null);
        }

        protected void Application_End()
        {
            //Comum.Log.RegistraTxt($"Parou a aplicação em {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}", null);
        }
    }
}
