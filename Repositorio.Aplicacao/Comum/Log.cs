﻿using Repositorio.Comum.Global;
using Repositorio.DAL.Repositorios;
using Repositorio.Entidades.Enlace;
using System;
using System.IO;

namespace Repositorio.Aplicacao.Comum
{
    public static class Log
    {
        private static string diretorioLog
        {
            get
            {
                if (!Directory.Exists(Path.Combine(ClsGlobal.DiretorioBase, "logFiles")))
                    Directory.CreateDirectory(Path.Combine(ClsGlobal.DiretorioBase, "logFiles"));

                return Path.Combine(ClsGlobal.DiretorioBase, "logFiles");
            }
        }

        public static void Erro(string observacao)
        {
            try
            {
                Erro(null, observacao);
            }
            catch (Exception exc)
            {
                RegistraTxt("Problema ao salvar log no banco", exc);
                throw;
            }
        }

        public static void Erro(Exception ex, string observacao)
        {
            try
            {
                var log = new TB_Log()
                {
                    Exception = GetException(ex),
                    InnerException = ex?.InnerException?.Message ?? string.Empty,
                    StackTrace = ex?.StackTrace?.ToString() ?? string.Empty,
                    Observacao = observacao ?? string.Empty,
                    Date = DateTime.Now
                };

                var db = new TB_LogRepositorio();
                db.Add(log);
                db.SaveAllChanges();
            }
            catch (Exception exc)
            {
                RegistraTxt($"Problema ao salvar log no banco | {observacao} | Ex: {exc.Message}", ex);
            }
        }

        public static void RegistraTxt(string erro, Exception ex)
        {
            var data = DateTime.Now.ToString("yyyyMMdd HHmmss");
            using (StreamWriter str = new StreamWriter(Path.Combine(diretorioLog, "Erro " + data + ".txt")))
            {
                str.WriteLine($"Observacao: {erro} | {GetException(ex)}");
                str.Flush();
            }
        }

        private static string GetException(Exception ex)
        {
            if (ex == null)
                return string.Empty;

            string mensagem = ex?.Message ?? string.Empty;
            string innerException = ex?.InnerException?.Message ?? string.Empty;
            string stacktrace = ex?.StackTrace?.ToString() ?? string.Empty;

            return $"Exception: {mensagem} | InnerException: {innerException} | StackTrace: {stacktrace}";
        }
    }
}