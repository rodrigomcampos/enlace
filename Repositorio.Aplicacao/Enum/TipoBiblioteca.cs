﻿public enum TipoBiblioteca : int
{
    Livro = 1,
    Artigo = 2,
    Video = 3
}