﻿public enum CursosEnum : int
{
    Nenhum = 0,
    ConstituicaoSujeitoIntervencoesTeoriaPraticaPsicanalitica = 1,
    NascimentoConstrucaoMente = 2,
    SeminariosClinicos = 3,
    FundamentosPsicanaliticosTeoriaTecnica = 4
}