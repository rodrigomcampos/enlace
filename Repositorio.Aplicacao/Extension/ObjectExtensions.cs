﻿namespace Repositorio.Aplicacao.Extension
{
    public static class ObjectExtensions
    {
        public static T GetValueObj<T>(this object obj, string campo)
        {
            try
            {
                return (T)obj.GetType().GetProperty(campo).GetValue(obj);
            }
            catch
            {
                return default(T);
            }
        }
    }
}