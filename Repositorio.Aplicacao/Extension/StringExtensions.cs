﻿using System.Text.RegularExpressions;

namespace Repositorio.Aplicacao.Extension
{
    public static class StringExtensions
    {
        public static string AsPropertyName(this string source)
        {
            return char.ToUpper(source[0]) + source.Substring(1);
        }

        public static bool ContainsIgnoringCase(this string source, string substring)
        {
            return source.ToLower().Contains(substring.ToLower());
        }

        public static bool IsEmpty(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }
        
        public static bool IsNotEmpty(this string str)
        {
            return str.IsEmpty() == false;
        }

       
        public static string NotEmptyOrNull(this string str)
        {
            return str.IsEmpty() ? null : str;
        }
        
        public static string SomenteNumeros(this string str)
        {
            if (str.IsEmpty())
                return str;

            return Regex.Replace(str, @"[^\d]", string.Empty);
        }
    }
}