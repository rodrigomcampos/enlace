var opcoes = (function ($) {

    function carregaEstados(elem) {
        return util.getOptionsSelect({
            url: "Estado",
            elemento: $(elem),
            codigo: "IdEstado",
            nome: "Nome"
        });
    }

    function carregaCidadesDoEstado(elem, idEstado) {
        return util.getOptionsSelect({
            url: "Cidade/" + idEstado,
            elemento: $(elem),
            codigo: "Ibge",
            nome: "Nome"
        });
    }

    function carregaTurmas(elem, curso) {
        $.ajax({
            url: 'curso/' + curso + '/dados',
            method: 'GET',
            contentType: "application/json",
            dataType: "json"
        }).done(function (r) {
            $(elem).append("<option value=''></option>");
            $.each(r, function (i) {
                $.each(r[i].Turmas, function (index) {
                    $(elem).append("<option value='" + r[i].Turmas[index].IdTurma + "'>" + r[i].Turmas[index].DiaSemana + "</option>");
                });
            });
        });
    }

    return {
        carregaEstados: carregaEstados,
        carregaCidadesDoEstado: carregaCidadesDoEstado,
        carregaTurmas: carregaTurmas
    }

})(jQuery);