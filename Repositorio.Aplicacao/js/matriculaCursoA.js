﻿$(document).ready(function () {

	function dadosValidos(classe) {
		let valido = true;
		$("input, select").filter("[required]").each(function () {
			if ($(this).val() == '' && $(this).val() !== 0) {
				valido = false;
			}
		});
		return valido;
	};

	function dadosMatricula(curso) {
		return {
			Curso: curso,
			NomeAluno: util.valorOuNulo($("#matricula_nome_completo")),
			Email: util.valorOuNulo($("#matricula_email")),
			Profissao: util.valorOuNulo($("#matricula_profissao")),
			Instituicao: util.valorOuNulo($("#matricula_instituição")),
			AnoFormacao: util.valorOuNulo($("#matricula_formacao")),
			NomeEmpresaTrabalha: util.valorOuNulo($("#matricula_empresa")),
			CPF: util.valorOuNulo($("#matricula_cpf")),
			CEP: util.valorOuNulo($("#matricula_cep")),
			Logradouro: util.valorOuNulo($("#matricula_logradouro")),
			Numero: util.valorOuNulo($("#matricula_numero")),
			Complemento: util.valorOuNulo($("#matricula_complemento")),
			Bairro: util.valorOuNulo($("#matricula_bairro")),
			IbgeCidade: util.valorOuNulo($("#matricula_cidade")),
			Telefone: util.valorOuNulo($("#matricula_contato")),
			Celular: util.valorOuNulo($("#matricula_contato")),
			Turma: util.valorOuNulo($('#matricula_turma')),
			Local: util.valorOuNulo($('#matricula_local'))
		}
	}

	function inscreverCurso(curso) {
		$.ajax({
			url: 'Cursos/inscrever',
			method: 'POST',
			data: JSON.stringify(dadosMatricula(curso)),
			contentType: "application/json",
			dataType: "json"
		}).done(function (r) {
			aviso.exibe(r);
			util.escondeModal();
		}).fail(function (r) {
			aviso.exibeMensagens(r.responseJSON);
			util.escondeModal();
		}).always(function (r) {
			$(".loading").hide();
		});
	}

	$("#matricula_cadastrar").click(function () {

		$(".loading").fadeIn(300);

		if (!dadosValidos()) {
			aviso.exibe("Os campo com '*' ao lado são obrigatórios.");
			$("input:required, .select_obrigatorio .select2-selection").addClass("campo_obrigatorio");
			$(".loading").hide();
		} else {
			inscreverCurso($("#matricula_curso").val());
		}

	});

});