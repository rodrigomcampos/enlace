$(document).ready(function () {

    $(".loading").fadeIn(200);

    $.get("evento", function (r) {

        $.each(r, function (i) {

            var conteudo = '<h4 class="titulo_info_evento">Evento: ' + r[i].NomeEvento + '</h4>' +
                '<ul class="dados_evento">' +
                '<li>' +
                '<img src="images/data.svg" alt="data" /> Data: ' + moment(r[i].DataEvento, moment.ISO_8601).format('DD/MM/YYYY') +
                '</li>' +
                '<li>' +
                '<img src="images/horario.svg" alt="horario" /> Horário: ' + r[i].Horario +
                '</li>' +
                '<li>' +
                '<img src="images/local.svg" alt="local" /> Local: ' + r[i].Local +
                '</li>' +
                '</ul>';

            var conteudoImagem = '<div style="background-image: url(' + r[i].UrlImagem + ');" class="espaco_evento crianca_evento">' +
                '<div class="capa_evento">' +
                '<div class="tarja_baixar">' +
                '<a href="' + r[i].UrlDocumento + '" target="_blank">' +
                r[i].NomeEvento +
                '<br>' +
                '<span class="baixar_link">Baixar Publicação</span>' +
                '</a>' +
                '</div>' +
                '</div>' +
                '<a class="botao_generico inscrever_evento" href="' + r[i].LinkFormulario + '" target="_blank">Inscreva-se</a>' +
                '</div>';

            $("#info_extra_evento").append(conteudo);

            $(".evento_imagem").append(conteudoImagem);

        });

        $(".loading").fadeOut(200);

    });
});