$(document).ready(function () {

    $.get("curso/3/dados").done(function (r) {

        $("#imagem_curso_modal").attr("src", r.CaminhoImagem);
        $("#sobre_modal").html(r.Sobre);
        $("#duracao_modal").html(r.Unidades[0].Duracao);
        $("#carga_horaria_modal").html(r.Unidades[0].CargaHoraria);
        $("#titulo_modal").html(r.Nome);
        $("#valor_matricula_modal").html(r.Unidades[0].ValorMatricula);
        $("#valor_parcela_modal").html(r.Unidades[0].ValorParcelas);
        $("#qtd_parcela_modal").html(r.Unidades[0].QuantidadeParcelas);
        $("#turmas_modal").empty();

        $.each(r.Unidades[0].Turmas, function (i) {

            var conteudo = '<p>' +
                '<span class="titulo_interno titulo_turma">TURMA ' + r.Unidades[0].Turmas[i].DiaSemana + '</span>' +
                '</p>' +
                '<p>' +
                '<span class="titulo_interno">Início: </span> ' + moment(r.Unidades[0].Turmas[i].DataInicio, moment.ISO_8601).format('DD/MM/YYYY') +
                '</p>' +
                '<p>' +
                '<span class="titulo_interno">Horário: </span> ' + r.Unidades[0].Turmas[i].DiaSemana + ' | ' + r.Unidades[0].Turmas[i].Horario +
                '</p>' +
                '<hr>';


            $("#turmas_modal").append(conteudo);

        });

        $(".loading").fadeOut(200);

    });

    function carregarMatricula() {
        $.ajax({
            url: "MatriculaCursoA.html",
            method: 'GET',
            contentType: "text/html",
            dataType: "html"
        }).done(function (r) {

            $.get("curso/3/dados").done(function (r) {
                $("#matricula_turma").empty();
                $.each(r.Unidades[0].Turmas, function (i) {
                    $("#matricula_turma").append("<option value='" + r.Unidades[0].Turmas[i].IdTurma + "'>" + r.Unidades[0].Turmas[i].DiaSemana + "</option>");
                });
            });

            util.configMatricula(r, 3);

            $("#matricula_curso").val(3).change();

            $("#matricula_local").val("Piracicaba").change();

            $(".modal_curso_expansao").fadeOut(300, function () {
                util.exibemodal(".modal_curso");
            });

            $(".fechar_modal").removeClass("fechar_modal_expansao").addClass("fechar_modal_curso");


        }).fail(function () {
            aviso.exibe("Erro inesperado. Contate o adminstrador do sistema.");
        });
    }

    $("#matricula_curso_exp_b").click(carregarMatricula);

});