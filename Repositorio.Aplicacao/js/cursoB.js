$(document).ready(function () {

    $("#lista_documentos").empty();

    function infoCurso(r, local) {
        $("#valor_parcela_" + local).html(r.ValorParcelas);
        $("#valor_matricula_" + local).html(r.ValorMatricula);
        $("#qtd_parcela_" + local).html(r.QuantidadeParcelas);
        $("#duracao_" + local).html(r.Duracao);
        $("#carga_horaria_" + local).html(r.CargaHoraria);
        $("#localizacao_" + local).html(r.EnderecoRealizacao);
    }

    $.get("curso/4/dados").done(function (r) {

        const unidades = r.Unidades;
        const documentos = r.DocumentosNecessarios;

        $("#sobre").html(r.Sobre);
        $("#informacoes").html(r.Informacoes);
        $("#imagem_curso").attr("src", "images/fund_psic.jpg");
        $("#destinado").html(r.Destinado);
        $("#titulo").html(r.Nome);
        $("#turmas").empty();

        $.each(documentos, function (i) {
            let conteudo = '<li>' + documentos[i].Descricao + ';</li>';
            $("#lista_documentos").append(conteudo);
        });

        $.each(unidades, function (i) {

            const turmas = unidades[i].Turmas;

            if (i == 0)
                infoCurso(unidades[0], "piracicaba");

            $.each(turmas, function (j) {

                let conteudo = '<p>' +
                    '<span class="titulo_interno titulo_turma">TURMA ' + turmas[j].DiaSemana + '</span>' +
                    '</p>' +
                    '<p>' +
                    '<span class="titulo_interno">Início: </span> ' + moment(turmas[j].DataInicio, moment.ISO_8601).format('DD/MM/YYYY') +
                    '</p>' +
                    '<p>' +
                    '<span class="titulo_interno">Horário: </span> ' + turmas[j].DiaSemana + ' | ' + turmas[j].Horario +
                    '</p>' +
                    '<hr>';

                if (i == 0)
                    $("#turmas_piracicaba").append(conteudo);

            });

        });

        $(".loading").fadeOut(200);

    });

    function carregarMatricula(local) {
        $.ajax({
            url: "MatriculaCursoA.html",
            method: 'GET',
            contentType: "text/html",
            dataType: "html"
        }).done(function (r) {

            $.get("curso/4/dados").done(function (r) {

                $("#matricula_turma").empty();

                if (local == 1)
                    $.each(r.Unidades[0].Turmas, function (i) {
                        $("#matricula_turma").append("<option value='" + r.Unidades[0].Turmas[i].IdTurma + "'>" + r.Unidades[0].Turmas[i].DiaSemana + "</option>");
                    });
                else
                    $.each(r.Unidades[1].Turmas, function (i) {
                        $("#matricula_turma").append("<option value='" + r.Unidades[1].Turmas[i].IdTurma + "'>" + r.Unidades[1].Turmas[i].DiaSemana + "</option>");
                    });

            });

            util.configMatricula(r, 4);

            $("#matricula_curso").val(4).change();

            if (local == 1)
                $("#matricula_local").val("Piracicaba").change();

            util.exibemodal(".modal_curso");

            $(".fechar_modal").addClass("fechar_modal_curso");


        }).fail(function () {
            aviso.exibe("Erro inesperado. Contate o adminstrador do sistema.");
        });
    }

    $("#matricula_curso_a_piracicaba, #matricula_curso_a_piracicaba_mob").click(function () {
        carregarMatricula(1);
    });

});