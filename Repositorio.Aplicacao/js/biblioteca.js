$(document).ready(function () {

    $(".loading").fadeOut(200);

    $(".mais_arquivos").off("click").click(function () {

        $(".fechar_modal").addClass("fechar_modal_biblioteca");

        util.exibemodal(".modal_biblioteca");

        $("#enviar_email_biblioteca").off("click").click(function () {

            $(".loading").fadeIn(400);

            $.ajax({
                url: 'biblioteca/solicitar',
                method: 'POST',
                data: JSON.stringify($("#email_biblioteca").val()),
                contentType: "application/json",
                dataType: "json"
            }).done(function (r) {
                aviso.exibe(r);
                util.escondeModal();
                $(".loading").fadeOut(300);
            }).fail(function () {
                aviso.exibe("Falha ao enviar email.");
                $(".loading").fadeOut(300);
            });
        });
    });

    configuraDatatables();

    dataTablesBiblioteca();

    function configuraDatatables() {
        $.extend(true, $.fn.dataTable.defaults, {
            "autoWidth": true,
            bLengthChange: false,
            scrollX: true,
            order: [
                [1, "asc"]
            ],
            iDisplayLength: 15,
            select: {
                style: 'api',
                info: false
            },
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "_": "%d  linhas selecionadas",
                        "0": "Clique na linha para seleciona-la",
                        "1": "1 linha selecionada"
                    }
                }
            }
        });

        $.fn.dataTable.ext.errMode = 'none';
    }

    function dataTablesBiblioteca() {

        var tabela_biblioteca = $("#biblioteca_dados").DataTable({
            ajax: {
                url: "biblioteca/arquivos",
                dataSrc: ""
            },
            columns: [{
                title: "Tipo do Arquivo",
                data: "Tipo"
            },
            {
                title: "Título",
                data: "Titulo"
            },
            {
                title: "Autor",
                data: "Autor"
            },
            {
                title: "Baixar",
                data: "Arquivo",
                render: function (data, type) {
                    return '<a target="_blank" href="' + data + '" title="Clique para baixar esse arquivo" class="baixar_arquivo">Baixar</a>';
                }
            }
            ]
        });

    };

});