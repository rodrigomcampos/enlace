$(document).ready(function () {

    $(".loading").fadeOut(200);

    function dadosEmail() {
        return
    }

    $("#enviar_msg").click(function () {

        $(".loading").fadeIn(300);

        var dados = {
            NomeCompleto: util.valorOuNulo($("#nome_contato")),
            Assunto: util.valorOuNulo($("#assunto_contato")),
            Email: util.valorOuNulo($("#email_contato")),
            Telefone: util.valorOuNulo($("#telefone_contato")),
            Mensagem: util.valorOuNulo($("#texto_contato"))
        };

        if (dados.NomeCompleto.length == null || dados.Email.length == null || dados.Mensagem.length == null) {
            aviso.exibe("Os campo com '*' ao lado são obrigatórios.").then(function () {
                $("input:required, #texto_contato").addClass("campo_obrigatorio");
                $(".loading").fadeOut(300);
            });
        } else {
            $.ajax({
                url: 'Contato/Enviar',
                method: 'POST',
                data: JSON.stringify(dadosEmail()),
                contentType: "application/json",
                dataType: "json"
            }).done(function (r) {
                aviso.exibe(r);
                $("input[type='text'], #texto_contato").val("").removeClass("campo_obrigatorio");
                util.escondeModal();
                $(".loading").fadeOut(300);
            }).fail(function () {
                aviso.exibe("Falha ao enviar mensagem.");
                $(".loading").fadeOut(300);
            });
        }

    });

});