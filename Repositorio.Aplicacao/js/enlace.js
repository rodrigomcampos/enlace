$(document).ready(function () {

    $.vegas.isVideoCompatible = function () {
        var devices = /(Android|webOS|Phone|iPad|iPod|BlackBerry|Windows Phone)/i;
        return !devices.test(navigator.userAgent);
    };

    $(".slogan_header").vegas({
        delay: 15000,
        slides: [
            {
                src: "images/curso_slide.jpg"
            },
            {
                src: "images/curso_b_slide.jpg"
            }
        ]
    });

    function eventosSlide(slideAtual) {
        if (slideAtual === 0)
            $(".principal_click").off("click").on("click", util.carregarPagina("CursoA.html", ".curso_a_div", "#cursos_link", ".evento_div, .contato_div, .sobre_nos_div"));
        else
            $(".principal_click").off("click").on("click", util.carregarPagina("CursoB.html", ".curso_a_div", "#cursos_link", ".evento_div, .contato_div, .sobre_nos_div"));
    }

    $("#prox").off("click").on("click", function () {
        $(".slogan_header").vegas("next");
        eventosSlide($(".slogan_header").vegas('current'));
    });

    $("#ante").off("click").on("click", function () {
        $(".slogan_header").vegas("previous");
        eventosSlide($(".slogan_header").vegas('current'));
    });

    eventosSlide($(".slogan_header").vegas('current'));

    function proximoSlide() {
        $("#prox").click();
    }

    setInterval(proximoSlide, 15000);

    function voltarHome() {
        scrollDiv("header", 0);
        $("header").show();
        $("nav").removeClass("nav_fix");
        $("nav a").removeClass("selecionado");
        $("#home_link").addClass("selecionado");
        $(".sobre_nos_div, .biblioteca_div, .curso_a_div, .contato_div, .evento_div, .logo_nav").empty().fadeOut();
        $("body").css("overflow-y", "auto");
        $(".atalho_contato").show();
    }

    function scrollDiv(div, n) {
        $("html, body").animate({
            scrollTop: $(div).offset().top - n
        }, 300);
        if ($("nav").hasClass("nav_mobile")) {
            esconderNav();
        }
    }

    $("#cursos_link").click(function () {
        $(".dropdown_biblioteca").hide();
        $(".dropdown_curso").slideToggle();
    });

    $("#biblioteca_link").click(function () {
        $(".dropdown_curso").hide();
        $(".dropdown_biblioteca").slideToggle();
    });

    $(window).click(function () {
        $(".dropdown_curso, .dropdown_biblioteca").slideUp();
    });

    $("#cursos_link, .dropdown_curso, #biblioteca_link, .dropdown_biblioteca, .indicador_slide").click(function (event) {
        event.stopPropagation();
    });

    $("#curso_a, #curso_a_drop").click(util.carregarPagina("CursoA.html", ".curso_a_div", "#cursos_link", ".biblioteca_div, .evento_div, .contato_div, .sobre_nos_div"));

    $("#curso_b, #curso_b_drop").click(util.carregarPagina("CursoB.html", ".curso_a_div", "#cursos_link", ".biblioteca_div, .evento_div, .contato_div, .sobre_nos_div"));

    $("#evento_link, #evento_slide").click(util.carregarPagina("Eventos.html", ".evento_div", "#evento_link", ".biblioteca_div, .curso_a_div, .sobre_nos_div, .contato_div"));
    $("#biblioteca_link").click(util.carregarPagina("Biblioteca.html", ".biblioteca_div", "#biblioteca_link", ".evento_div, .curso_a_div, .sobre_nos_div, .contato_div"));
    $("#quem_somos_link").click(util.carregarPagina("SobreNos.html", ".sobre_nos_div", "#quem_somos_link", ".biblioteca_div, .evento_div, .contato_div, .curso_a_div"));
    $("#contato_link").click(util.carregarPagina("ContatoPagina.html", ".contato_div", "#contato_link", ".biblioteca_div, .evento_div, .curso_a_div, .sobre_nos_div"));

    $(".atalho_contato").click(util.carregarModal("ContatoModal.html", ".modal_contato"));

    $("#curso_exp_a, #exp_curso_a_drop").click(util.carregarModal("CursoExpansao_A.html", ".modal_curso_expansao"));

    $("#curso_exp_b, #exp_curso_b_drop").click(util.carregarModal("CursoExpansao_B.html", ".modal_curso_expansao"));

    $(".bg_modal, .fechar_modal").click(util.escondeModal);

    $("#home_link").click(function () {
        voltarHome();
    });

});