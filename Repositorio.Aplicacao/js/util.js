var util = (function ($) {

    function getOptionsSelect(opcoes) {
        var q = $.Deferred();

        var opt = $.extend({}, {
            url: '',
            data: null,
            elemento: $({}),
            codigo: 'id',
            alt: 'alt',
            nome: 'nome',
            itemPadrao: '',
            itemVazio: ''
        }, opcoes);

        if (opt.elemento) {

            $.get(opt.url, opt.data)
                .done(function (itens) {
                    opt.elemento.empty();
                    var option = $('<option/>');
                    var options = (itens || []).map(function (item) {
                        return option
                            .clone()
                            .attr('value', getValueOf(item, opt.codigo))
                            .attr('alt', getValueOf(item, opt.alt))
                            .text(getValueOf(item, opt.nome));
                    });

                    if (options.length == 0 && opt.itemVazio !== '')
                        opt.elemento.append(option.clone().attr('value', 0).text(opt.itemVazio));
                    else if (opt.itemPadrao !== '')
                        opt.elemento.append(option.clone());
                    //opt.elemento.append(option.clone().attr('value', null).text(opt.itemPadrao));

                    opt.elemento.append(option.clone()).append(options);

                })
                .always(q.resolve);
        } else {
            q.resolve();
        }

        return q.promise();
    }

    function getValueOf(item, opcao) {
        if (typeof opcao == 'function') {
            return opcao(item);
        } else {
            return item[opcao];
        }
    }

    function aplicarSelect2(id) {
        $(id).select2({
            language: "pt-BR",
            allowClear: true,
            minimumResultsForSearch: 5,
            tags: false
        });
    }

    function dateTimePicker(input) {
        $(input).datetimepicker({
            format: "d/m/Y",
            step: 1,
            timepicker: false
        });
        $.datetimepicker.setLocale('pt-BR');
        $(input).keypress(function () {
            event.preventDefault();
        })
    }

    function valorOuNulo(elem) {
        var val = elem.val();
        return (val == "") ? null : val;
    }

    function limpa_formulario_cep(rua, bairro) {
        $(rua + "," + bairro).val("");
    }

    function buscaCEP(cep, bairro, rua) {

        cep = $(cep).val().replace(/\D/g, '');

        if (cep !== "") {

            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $(rua).val("...");
                $(bairro).val("...");

                $.getJSON("http://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
                    if (!("erro" in dados)) {

                        $(rua).val(dados.logradouro);

                        $(bairro).val(dados.bairro);

                    } else {
                        limpa_formulario_cep(rua, bairro);
                        aviso.exibe("CEP não encontrado.");
                    }
                });
            } else {
                limpa_formulario_cep(rua, bairro);
                aviso.exibe("CEP inválido.");
            }
        } else {
            limpa_formulario_cep(rua, bairro);
        }
    }

    function fadeInTela(click, show, hide) {

        $("nav a").removeClass("selecionado");

        $(".logo_nav").show();

        $(click).addClass("selecionado");

        $(show).fadeIn(200, function () {
            $(hide + ", header").hide();
            $(hide).empty();
        });

        $("nav").addClass("nav_fix");

        $(show).animate({
            scrollTop: $("header").offset().top
        });

        $("body").css("overflow", "hidden");

        if (click == "#contato_link") {
            $(".atalho_contato").hide();
        } else {
            $(".atalho_contato").show();
        }

    }

    function exibemodal(div) {
        $(".bg_modal, " + div).fadeIn(300);
        $(".atalho_contato, nav, header, section, iframe, footer").addClass("blur");
        $(".dropdown_curso").slideUp();
        $("body").css("overflow-y", "hidden");
    }

    function escondeModal() {
        $(".bg_modal, .modal_contato, .modal_curso, .modal_curso_expansao, .modal_biblioteca").fadeOut(300, function () {
            $(".atalho_contato, nav, header, section, iframe, footer").removeClass("blur");
            $(".fechar_modal").removeClass("fechar_modal_curso fechar_modal_expansao fechar_modal_biblioteca");
            if (!$(".sobre_nos_div").is(':visible'))
                $("body").css("overflow-y", "auto");
        });
    }

    function carregarPagina(url, div, click, hide) {
        return function () {
            $(".loading").fadeIn(300, function () {
                $.ajax({
                    url: url,
                    method: 'GET',
                    contentType: "text/html",
                    dataType: "html"
                }).done(function (r) {
                    $(div).html(r);
                    fadeInTela(click, div, hide);
                    $(".dropdown_curso, .dropdown_biblioteca").slideUp();
                    $(".loading").fadeOut(300);
                }).fail(function () {
                    aviso.exibe("Erro inesperado. Contate o adminstrador do sistema.");
                });
            });
        }
    }

    function carregarModal(url, div, click, hide) {
        return function () {

            $(".loading").fadeIn(300);

            $.ajax({
                url: url,
                method: 'GET',
                contentType: "text/html",
                dataType: "html"
            }).done(function (r) {

                $(div).html(r);

                exibemodal(div);

                if (div == ".modal_curso_expansao")
                    $(".fechar_modal").addClass("fechar_modal_expansao");

            }).fail(function () {
                aviso.exibe("Erro inesperado. Contate o adminstrador do sistema.");
            });
        }
    }

    function configMatricula(r, curso) {

        $(".modal_curso").html(r);

        $("#matricula_cpf").inputmask("999.999.999-99", {
            autoUnmask: true
        });

        $("#matricula_cep").inputmask("99999-999", {
            autoUnmask: true
        });

        $("#matricula_cep").blur(function () {
            util.buscaCEP(this, "#matricula_bairro", "#matricula_logradouro");
        });

        $.when(
            opcoes.carregaTurmas("#matricula_turma", curso),
            opcoes.carregaEstados("#matricula_uf")
        ).then(function () {
            util.aplicarSelect2(".select2_html");
        });

        $("#matricula_uf").change(function () {
            $(".loading").fadeIn(300);
            opcoes.carregaCidadesDoEstado("#matricula_cidade", $(this).val()).then(function () {
                util.aplicarSelect2("#matricula_cidade");
                $(".loading").fadeOut(300);
            });
        });

        util.aplicarSelect2(".select2_html");

    }

    return {
        getOptionsSelect: getOptionsSelect,
        dateTimePicker: dateTimePicker,
        valorOuNulo: valorOuNulo,
        buscaCEP: buscaCEP,
        aplicarSelect2: aplicarSelect2,
        fadeInTela: fadeInTela,
        carregarPagina: carregarPagina,
        carregarModal: carregarModal,
        exibemodal: exibemodal,
        escondeModal: escondeModal,
        configMatricula: configMatricula
    }

})(jQuery);