﻿using Repositorio.Aplicacao.Comum;
using System;
using System.Net;
using System.Net.Mail;

namespace Repositorio.Aplicacao.Models
{
    public static class Email
    {
        public static void Enviar(string assunto, string corpoEmail)
        {
            try
            {
                EnviarEmailDominio(assunto, corpoEmail);
                EnviarEmailGmail(assunto, corpoEmail);
            }
            catch (Exception ex)
            {
                Log.Erro(ex, "Erro ao enviar e-mail");
            }
        }

        private static void EnviarEmailDominio(string assunto, string corpoEmail)
        {
            using (SmtpClient client = new SmtpClient("smtp.umbler.com"))
            {
                client.Port = 587;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("contato@enlacepiracicaba.com.br", "Enlace10203040");
                using (MailMessage mail = new MailMessage())
                {
                    mail.Sender = new MailAddress("contato@enlacepiracicaba.com.br", "Enlace Piracicaba");
                    mail.From = new MailAddress("contato@enlacepiracicaba.com.br", "Enlace Piracicaba");
                    mail.To.Add(new MailAddress("contato@enlacepiracicaba.com.br", "RECEBEDOR"));
                    mail.Subject = assunto;
                    mail.Body = corpoEmail;
                    mail.IsBodyHtml = true;
                    mail.Priority = MailPriority.High;
                    client.Send(mail);
                }
            }
        }

        private static void EnviarEmailGmail(string assunto, string corpoEmail)
        {
            using (SmtpClient client = new SmtpClient("smtp.gmail.com"))
            {
                client.Port = 587;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("enlacepiracicaba@gmail.com", "Enlace10203040");
                using (MailMessage mail = new MailMessage())
                {
                    mail.Sender = new MailAddress("enlacepiracicaba@gmail.com", "Enlace Piracicaba");
                    mail.From = new MailAddress("enlacepiracicaba@gmail.com", "Enlace Piracicaba");
                    mail.To.Add(new MailAddress("enlacepiracicaba@gmail.com", "RECEBEDOR"));
                    mail.Subject = assunto;
                    mail.Body = corpoEmail;
                    mail.IsBodyHtml = true;
                    mail.Priority = MailPriority.High;
                    client.Send(mail);
                }
            }
        }
    }
}