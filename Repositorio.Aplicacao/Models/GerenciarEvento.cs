﻿using Repositorio.Aplicacao.Comum;
using Repositorio.Aplicacao.Exceptions;
using Repositorio.DAL.Repositorios;
using System;
using System.Linq;
using System.Web;

namespace Repositorio.Aplicacao.Models
{
    public class GerenciarEvento
    {
        private HttpRequest request;
        private TB_EventoRepositorio repEvento { get; set; } = new TB_EventoRepositorio();

        private string nomeEvento { get; set; }
        private HttpPostedFile imagem { get; set; }
        private HttpPostedFile pdf { get; set; }
        private string tipoEvento { get; set; }
        private string formulario { get; set; }
        private string data { get; set; }
        private string horario { get; set; }
        private string local { get; set; }

        private string urlImagem { get; set; }
        private string urlPDF { get; set; }

        private bool ativo { get; set; }

        private bool temPDF => pdf != null;
        private bool temImagem => imagem != null;

        public GerenciarEvento(HttpRequest request)
        {
            this.request = request;
        }

        public void CadastrarEvento()
        {
            try
            {
                PopularVariveis();
                ValidarSeNomeJaExiste();
                SalvarArquivos();

                repEvento.Add(new Entidades.Enlace.TB_Evento()
                {
                    NomeEvento = nomeEvento,
                    UrlImagem = urlImagem,
                    UrlDocumento = urlPDF,
                    TipoEvento = tipoEvento,
                    LinkFormulario = formulario,
                    DataEvento = Convert.ToDateTime(data),
                    Horario = horario,
                    Local = local
                });

                repEvento.SaveAllChanges();
            }
            catch (ValidacaoException) { throw; }
            catch (Exception ex)
            {
                Log.Erro(ex, "Problema ao cadastrar evento");
                throw new ValidacaoException("Problema ao cadastrar evento");
            }
        }

        public void Editar(int idEvento)
        {
            try
            {
                PopularVariveis();
                SalvarArquivos();

                var evento = repEvento.GetAll().Where(t => t.IdEvento == idEvento).First();

                evento.NomeEvento = nomeEvento;

                if (temImagem)
                    evento.UrlImagem = urlImagem;

                if (temPDF)
                    evento.UrlDocumento = urlPDF;

                evento.TipoEvento = tipoEvento;
                evento.LinkFormulario = formulario;
                evento.DataEvento = Convert.ToDateTime(data);
                evento.Horario = horario;
                evento.Local = local;
                evento.Ativo = ativo;

                repEvento.Update(evento);
                repEvento.SaveAllChanges();
            }
            catch (ValidacaoException ex) { throw ex; }
            catch (Exception ex)
            {
                Log.Erro(ex, "Problema ao editar evento");
                throw new ValidacaoException("Problema ao editar evento");
            }
        }

        private void PopularVariveis()
        {
            try
            {
                nomeEvento = request.Form["Nome"];
                imagem = request.Files["Imagem"];
                pdf = request.Files["PDF"];
                tipoEvento = request.Form["TipoEvento"];
                formulario = request.Form["Formulario"];
                data = request.Form["Data"];
                horario = request.Form["Horario"];
                local = request.Form["Local"];

                if (temImagem)
                    urlImagem = $"../images/{imagem.FileName}";

                if (temPDF)
                    urlPDF = $"pdf/{pdf.FileName}";

                ativo = Convert.ToBoolean(request.Form["Ativo"]);
            }
            catch (Exception ex)
            {
                Log.Erro(ex, "Problema ao popular variaveis");
                throw;
            }
        }

        private void ValidarSeNomeJaExiste()
        {
            var jaExiste = (from ev in repEvento.GetAll()
                            where ev.NomeEvento == nomeEvento
                            && ev.Ativo == true
                            select ev).Count() > 0;

            if (jaExiste)
                throw new ValidacaoException($"Já existe um evento com este nome {nomeEvento} cadastrado");
        }

        private void SalvarArquivos()
        {
            try
            {
                if (temPDF)
                    pdf.SaveAs(HttpContext.Current.Server.MapPath("~/pdf/") + pdf.FileName);

                if (temImagem)
                    imagem.SaveAs(HttpContext.Current.Server.MapPath("~/images/") + imagem.FileName);
            }
            catch (Exception ex)
            {
                Log.Erro(ex, "Problema ao salvar arquivos");
                throw;
            }
        }
    }
}