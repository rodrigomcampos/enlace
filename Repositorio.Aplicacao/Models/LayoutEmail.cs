﻿using Repositorio.Aplicacao.Models.Request;
using Repositorio.Comum.Global;
using System.IO;

namespace Repositorio.Aplicacao.Models
{
    public class LayoutEmail
    {
        public string CriarHtml(ContatoRequestModel contato)
        {
            var diretorioArquivo = Path.Combine(ClsGlobal.DiretorioBase, "LayoutEmail.html");
            using (StreamReader str = new StreamReader(diretorioArquivo))
            {
                var layout = str.ReadToEnd();
                layout = layout.Replace("#EMAIL", contato.Email);
                layout = layout.Replace("#NOME", contato.NomeCompleto);
                layout = layout.Replace("#MENSAGEM", contato.Mensagem);
                layout = layout.Replace("#TELEFONE", contato.Telefone);
                return layout;
            }
        }
    }
}