﻿using Repositorio.Aplicacao.Comum;
using Repositorio.Aplicacao.Models.Request;
using Repositorio.DAL.Repositorios;
using Repositorio.Entidades.Enlace;
using System;
using System.Linq;

namespace Repositorio.Aplicacao.Models
{
    public class InscricaoCurso
    {
        private InscricaoCursoRequest Request { get; set; }
        private TB_Liga_Curso_Turma turma { get; set; }

        public InscricaoCurso(InscricaoCursoRequest request)
        {
            this.Request = request;
            turma = new TB_Turma_Repositorio().GetWhere(x => x.IdCursoTurma == request.Turma)?.FirstOrDefault();
        }

        public void AdicionarNoBanco()
        {
            try
            {
                var db = new TB_Liga_Curso_InscricaoRepositorio();
                db.Add(new TB_Liga_Curso_Inscricao
                {
                    xIdCurso = (int)Request.Curso,
                    NomeAluno = Request.NomeAluno,
                    Email = Request.Email,
                    Profissao = Request.Profissao,
                    Instituicao = Request.Instituicao,
                    AnoFormacao = Request.AnoFormacao,
                    NomeEmpresaTrabalha = Request.NomeEmpresaTrabalha,
                    CPF = Request.CPF,
                    CEP = Request.CEP,
                    Logradouro = Request.Logradouro,
                    Numero = Request.Numero,
                    Complemento = Request.Complemento,
                    Bairro = Request.Bairro,
                    IbgeCidade = Request.IbgeCidade,
                    Telefone = Request.Telefone,
                    Celular = Request.Celular,
                    DiaSemana = turma.DiaSemana,
                    DataInicio = turma.DataInicio,
                    Horario = turma.Horario,
                    Unidade = Request.Local
                });

                db.SaveAllChanges();
            }
            catch (Exception ex)
            {
                Log.Erro(ex, "Erro ao salvar inscrição no curso");
                throw;
            }
        }

        public void EnviarPorEmail()
        {
            try
            {
                var corpoEmail = new LayoutEmailInscricaoCurso().CriarHtml(Request, turma);
                var assunto = GetAsssunto();

                Email.Enviar(assunto, corpoEmail);
            }
            catch (Exception ex)
            {
                Log.Erro(ex, "Erro ao enviar inscrição do curso por e-mail");
                throw;
            }
        }

        private string GetAsssunto()
        {
            if (Request.Curso == CursosEnum.ConstituicaoSujeitoIntervencoesTeoriaPraticaPsicanalitica)
                return "Inscricão Curso: Constituição do Sujeito e Intervenções | Teoria e Prática Psicanalítica";
            else if (Request.Curso == CursosEnum.NascimentoConstrucaoMente)
                return "Inscricão Curso: Nascimento e Construção da Mente";

            return null;
        }
    }
}