﻿using Repositorio.Entidades.Enlace;

namespace Repositorio.Aplicacao.Models
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        public string Login { get; set; }
        public string Nome { get; set; }
        public TipoUsuario Tipo { get; set; }

        public static Usuario Converter(TB_Usuario usuario)
        {
            return new Usuario()
            {
                IdUsuario = usuario.IdUsuario,
                Login = usuario.Login,
                Nome = usuario.Nome,
                Tipo = (TipoUsuario)usuario.TipoUsuario
            };
        }
    }
}