﻿using Repositorio.Aplicacao.Comum;
using Repositorio.Aplicacao.Models.Request;
using Repositorio.Aplicacao.Models.Response;
using Repositorio.DAL.Repositorios;
using Repositorio.Entidades.Enlace;
using System;
using System.Data;
using System.Linq;

namespace Repositorio.Aplicacao.Models
{
    public class EditarCurso
    {
        private EditarCursoRequestModel request { get; set; }
        private TB_CursoRepositorio cursoRep { get; set; } = new TB_CursoRepositorio();
        private TB_Turma_Repositorio turmaRep { get; set; } = new TB_Turma_Repositorio();
        private TB_Liga_Curso_UnidadeRepositorio unidadeRep { get; set; } = new TB_Liga_Curso_UnidadeRepositorio();

        private TB_Liga_Curso_Documentos_Necessarios_Repositorio docNecRep { get; set; }

        public EditarCurso(EditarCursoRequestModel request)
        {
            this.request = request;
            this.docNecRep = new TB_Liga_Curso_Documentos_Necessarios_Repositorio();
        }

        public void Editar()
        {
            using (var trans = cursoRep.ctx.Database.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    var curso = AtualizarInformacoesCurso();

                    EditarUnidades(curso);
                    DeletarCadastrarDocumentos();

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    Log.RegistraTxt("Problema ao editar curso", ex);
                    throw ex;
                }
            }
        }

        private TB_Curso AtualizarInformacoesCurso()
        {
            var cursoModel = cursoRep.GetAll().Where(x => x.IdCurso == request.IdCurso).Single();

            cursoModel.Nome = request.Nome;
            cursoModel.Sobre = request.Sobre;
            cursoModel.Informacoes = request.Informacoes;
            cursoModel.Destinado = request.Destinado;
            cursoModel.Ativo = request.Ativo ? 1 : 0;

            cursoRep.Update(cursoModel);
            cursoRep.SaveAllChanges();

            GetCursoResponseModel.RemoverCursoCache(cursoModel.CodigoCurso);

            return cursoModel;
        }

        private void EditarUnidades(TB_Curso curso)
        {
            var turmas = (from turma in turmaRep.GetAll()
                          join unidade in turmaRep.ctx.Set<TB_Liga_Curso_Unidade>()
                            on turma.IdLigaCursoUnidade equals unidade.IdLigaCursoUnidade
                          where unidade.CodigoCurso == curso.CodigoCurso
                          select turma).ToList();

            foreach (var turmaDel in turmas)
                turmaRep.Delete(t => t.IdCursoTurma == turmaDel.IdCursoTurma);

            turmaRep.SaveAllChanges();

            unidadeRep.Delete(unidade => unidade.CodigoCurso == curso.CodigoCurso);

            foreach (var unidade in request.Unidades)
            {
                var dados = unidade.Dados;

                unidadeRep.Add(new TB_Liga_Curso_Unidade()
                {
                    CodigoCurso = curso.CodigoCurso,
                    IdCidade = unidade.Unidade == 3538709 ? 5136 : 4809,
                    CargaHoraria = dados.CargaHoraria,
                    ValorMatricula = dados.ValorMatricula.HasValue ? dados.ValorMatricula.Value : (decimal?)null,
                    QuantidadeParcelas = dados.QuantidadeParcelas,
                    ValorParcelas = dados.ValorParcelas.HasValue ? dados.ValorParcelas.Value : (decimal?)null,
                    EnderecoRealizacao = dados.EnderecoRealizacao,
                    Duracao = dados.Duracao,
                    Ativo = dados.Ativo,
                    TB_Liga_Curso_Turma = dados.Turmas.Select(turma => new TB_Liga_Curso_Turma()
                    {
                        DataInicio = turma.DataInicio,
                        DiaSemana = turma.DiaSemana,
                        Horario = turma.Horario
                    })
                    .ToList()
                });
            }

            unidadeRep.SaveAllChanges();
        }

        private void DeletarCadastrarDocumentos()
        {
            docNecRep.ctx = cursoRep.ctx;
            docNecRep.Delete(x => x.xIdCurso == request.IdCurso);

            foreach (var idDoc in request.IdsDocumentosNecessarios)
            {
                docNecRep.Add(new Entidades.Enlace.TB_Liga_Curso_Documentos_Necessarios()
                {
                    xIdCurso = request.IdCurso,
                    xIdDocumentosNecessarios = idDoc
                });
            }
            docNecRep.SaveAllChanges();
        }
    }
}