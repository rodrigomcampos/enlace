﻿using Repositorio.Aplicacao.Comum;
using Repositorio.Comum.Global;
using System;
using System.IO;

namespace Repositorio.Aplicacao.Models
{
    public class ArquivoBiblioteca
    {
        public void EnviarEmail(string email)
        {
            try
            {
                var corpo = CriarHtml(email);
                var assunto = "Solicitação de arquivo da biblioteca";
                Email.Enviar(assunto, corpo);
            }
            catch (Exception ex)
            {
                Log.Erro(ex, "Erro ao enviar e-mail de solicitação de arquivo da biblioteca");
                throw;
            }
        }

        public string CriarHtml(string email)
        {
            var diretorioArquivo = Path.Combine(ClsGlobal.DiretorioBase, "LLayoutEmailBiblioteca.html");
            using (StreamReader str = new StreamReader(diretorioArquivo))
            {
                var layout = str.ReadToEnd();
                layout = layout.Replace("#EMAIL", email);
                return layout;
            }
        }
    }
}