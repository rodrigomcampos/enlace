﻿using Newtonsoft.Json;
using Repositorio.Aplicacao.Extension;
using Repositorio.Comum.Global;
using Repositorio.DAL.Repositorios;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Repositorio.Aplicacao.Models.Response
{
    public static class GetCursoResponseModel
    {
        private static HashSet<object> _cursos { get; set; } = new HashSet<object>();

        public static object GetCurso(int codigoCurso)
        {
            var caminhoArquivo = GetNomeArquivo(codigoCurso);

            if (File.Exists(caminhoArquivo) == false)
            {
                _cursos.Add(new TB_CursoRepositorio().GetCurso(codigoCurso));
                CriarArquivo(caminhoArquivo, codigoCurso);
            }

            using (var str = new StreamReader(caminhoArquivo, Encoding.UTF8))
            {
                var teste = str.ReadToEnd();
                return JsonConvert.DeserializeObject<object>(teste);
            }
        }

        private static void CriarArquivo(string caminhoArquivo, int codigoCurso)
        {
            var curso = _cursos.First(t => t.GetValueObj<int>("CodigoCurso") == codigoCurso);
            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(curso);
            File.WriteAllText(caminhoArquivo, json);
        }

        private static string GetNomeArquivo(int codigoCurso)
        {
            return Path.Combine(ClsGlobal.DiretorioBase, "json", $"Curso{codigoCurso}.json");
        }

        public static void RemoverCursoCache(int codigoCurso)
        {
            var arquivo = GetNomeArquivo(codigoCurso);
            File.Delete(arquivo);
        }
    }
}