﻿using Repositorio.DAL.Repositorios;
using System.Collections.Generic;
using System.Linq;

namespace Repositorio.Aplicacao.Models.Response
{
    public class OpcoesResponse
    {
        public static IEnumerable<object> GetDocumentosNecessarios()
        {
            return new TB_Documentos_NecessariosRepositorio()
                                    .GetAll()
                                    .Select(t => new
                                    {
                                        Valor = t.IdDocumentosNecessarios,
                                        Texto = t.Descricao
                                    })
                                    .ToList();                                                                                
        }

        public static IEnumerable<object> GetTipoBiblioteca()
        {
            return new TB_Tipo_BibliotecaRepositorio()
                .GetAll()
                .Select(t => new
                {
                    Valor = t.Codigo,
                    Texto = t.Descricao
                })
                .ToList();
        }
    }
}