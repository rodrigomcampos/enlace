﻿using Repositorio.DAL.Repositorios;
using System.Linq;

namespace Repositorio.Aplicacao.Models.Response
{
    public class SelecionarEventoResponseModel
    {
        public int IdEvento { get; set; }
        public string NomeEvento { get; set; }
        public string TipoEvento { get; set; }
        public string UrlImagem { get; set; }
        public string UrlDocumento { get; set; }
        public string LinkFormulario { get; set; }
        public string DataEvento { get; set; }
        public string Horario { get; set; }
        public string Local { get; set; }

        public static SelecionarEventoResponseModel Get(int idEvento)
        {
            var repEv = new TB_EventoRepositorio();

            return (from ev in repEv.GetAll()
                    where ev.Ativo == true
                    && ev.IdEvento == idEvento
                    select new SelecionarEventoResponseModel()
                    {
                        IdEvento = ev.IdEvento,
                        NomeEvento = ev.NomeEvento,
                        TipoEvento = ev.TipoEvento,
                        UrlImagem = ev.UrlImagem,
                        UrlDocumento = ev.UrlDocumento,
                        LinkFormulario = ev.LinkFormulario,
                        DataEvento = ev.DataEvento.ToString(),
                        Horario = ev.Horario,
                        Local = ev.Local
                    }).FirstOrDefault();
        }
    }
}