﻿using Repositorio.Comum.Models;
using Repositorio.DAL.Repositorios;
using System.Collections.Generic;
using System.Linq;

namespace Repositorio.Aplicacao.Models.Response
{
    public class CidadeResponse
    {
        private static IEnumerable<Cidade> _cidades;

        public static IEnumerable<Cidade> Cidades(int idEstado)
        {
            if (_cidades == null)
                _cidades = new CidadeRepositorio().GetAll().Select(t => new Cidade()
                {
                    IdCidade = t.IdCidade,
                    Nome = t.Nome,
                    Ibge = t.Ibge,
                    IdEstado = t.IdEstado
                })
                .ToList();

            return _cidades.Where(t => t.IdEstado == idEstado).ToList();
        }
    }
}