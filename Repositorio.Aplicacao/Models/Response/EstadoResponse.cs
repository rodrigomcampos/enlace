﻿using Repositorio.Comum.Models;
using Repositorio.DAL.Repositorios;
using System.Collections.Generic;
using System.Linq;

namespace Repositorio.Aplicacao.Models.Response
{
    public static class EstadoResponse
    {
        private static IEnumerable<Estado> _estados;

        public static IEnumerable<Estado> Estados
        {
            get
            {
                if (_estados == null)
                    _estados = new EstadoRepositorio().GetEstados().ToList();

                return _estados;
            }
        }
    }
}