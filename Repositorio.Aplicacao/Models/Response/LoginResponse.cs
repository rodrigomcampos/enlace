﻿namespace Repositorio.Aplicacao.Models.Response
{
    public class LoginResponse
    {
        public string Token { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public int TipoUsuario { get; set; }
    }
}