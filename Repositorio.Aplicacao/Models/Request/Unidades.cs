﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Repositorio.Aplicacao.Models.Request
{
    public class Unidades
    {
        public long Unidade { get; set; }
        public DadosUnidades Dados { get; set; }
    }

    public class DadosUnidades
    {
        public decimal? ValorMatricula { get; set; }

        public int? QuantidadeParcelas { get; set; }

        public decimal? ValorParcelas { get; set; }

        [Required(ErrorMessage = "Duração é obrigatória")]
        public string Duracao { get; set; }

        [Required(ErrorMessage = "Carga Horária do Curso é obrigatória")]
        public int CargaHoraria { get; set; }        

        public string EnderecoRealizacao { get; set; }

        [Required(ErrorMessage = "Ativo é obrigatório")]
        public bool Ativo { get; set; }

        public ICollection<TurmaCurso> Turmas { get; set; }
    }

    public class TurmaCurso
    {
        public string DiaSemana { get; set; }

        public DateTime DataInicio { get; set; }

        public string Horario { get; set; }
    }
}