﻿using Repositorio.DAL.Repositorios;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Repositorio.Aplicacao.Models.Request
{
    public class CadastrarItemBibliotecaRequestModel
    {
        private TB_BibliotecaRepositorio Biblioteca { get; set; } = new TB_BibliotecaRepositorio();

        [StringLength(200, MinimumLength = 1, ErrorMessage = "Título da biblioteca inválido")]
        public string Titulo { get; set; }

        public string Autor { get; set; }

        [StringLength(200, MinimumLength = 1, ErrorMessage = "Arquivo inválido")]
        public string Arquivo { get; set; }

        [Required(ErrorMessage = "Tipo é obrigatório")]
        public TipoBiblioteca Tipo { get; set; }

        public void Cadastrar()
        {
            Biblioteca.Add(new Entidades.Enlace.TB_Biblioteca()
            {
                Titulo = this.Titulo,
                Autor = this.Autor,
                Arquivo = this.Arquivo,
                CodigoTipo = (int)this.Tipo
            });

            Biblioteca.SaveAllChanges();
        }

        public bool ValidarSeJaExiste()
        {
            return Biblioteca.GetWhere(t => t.Titulo == this.Titulo).Count() > 0;
        }
    }
}