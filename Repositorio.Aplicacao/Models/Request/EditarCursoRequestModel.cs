﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Repositorio.Aplicacao.Models.Request
{
    public class EditarCursoRequestModel
    {
        [Required(ErrorMessage = "IdCurso do Curso é obrigatório")]
        public int IdCurso { get; set; }

        [Required(ErrorMessage = "Nome do Curso é obrigatório")]
        [StringLength(100)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Sobre o Curso é obrigatório")]
        public string Sobre { get; set; }

        public string Informacoes { get; set; }

        public string Destinado { get; set; }

        public List<Unidades> Unidades { get; set; }

        [Required(ErrorMessage = "Ativo é obrigatório")]
        public bool Ativo { get; set; }
        
        public ICollection<int> IdsDocumentosNecessarios { get; set; }
    }
}