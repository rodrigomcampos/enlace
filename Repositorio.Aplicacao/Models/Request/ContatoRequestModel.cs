﻿using System.ComponentModel.DataAnnotations;

namespace Repositorio.Aplicacao.Models.Request
{
    public class ContatoRequestModel
    {
        [Required(ErrorMessage = "Nome é obrigatório para o contato")]
        public string NomeCompleto { get; set; }

        [Required(ErrorMessage = "Assunto é obrigatório para o contato")]
        public string Assunto { get; set; }

        [Required(ErrorMessage = "O e-mail é obrigatório para o contato")]
        [EmailAddress(ErrorMessage = "O e-mail informado é inválido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "A mensagem é obrigatória para o contato")]
        public string Mensagem { get; set; }

        public string Telefone { get; set; }
    }
}