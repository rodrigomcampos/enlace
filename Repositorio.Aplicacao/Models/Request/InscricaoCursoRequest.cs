﻿using Repositorio.Aplicacao.Validate;
using System.ComponentModel.DataAnnotations;

namespace Repositorio.Aplicacao.Models.Request
{
    public class InscricaoCursoRequest
    {
        [Required(ErrorMessage = "Código do curso é obrigatório")]
        public CursosEnum Curso { get; set; }

        [Required(ErrorMessage = "Nome do aluno é obrigatório")]
        public string NomeAluno { get; set; }

        [Email(ErrorMessage = "E-mail inválido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Profissão do aluno é obrigatória")]
        public string Profissao { get; set; }

        public string Instituicao { get; set; }

        public int AnoFormacao { get; set; }

        public string NomeEmpresaTrabalha { get; set; }

        [Required(ErrorMessage = "CPF do aluno inválido")]
        [Cpf(ErrorMessage = "CPF do aluno inválido")]
        public string CPF { get; set; }

        [Required(ErrorMessage = "CEP é obrigatório")]
        public string CEP { get; set; }

        [Required(ErrorMessage = "Logradouro é obrigatório")]
        public string Logradouro { get; set; }

        [Required(ErrorMessage = "Número é obrigatório")]
        public int Numero { get; set; }

        public string Complemento { get; set; }

        [Required(ErrorMessage = "Bairro é obrigatório")]
        public string Bairro { get; set; }

        [Required(ErrorMessage = "Ibge da cidade é obrigatório")]
        public int IbgeCidade { get; set; }

        public string Telefone { get; set; }

        public string Celular { get; set; }

        [Required(ErrorMessage = "Turma é obrigatória")]
        public int Turma { get; set; }

        [Required(ErrorMessage = "Local é obrigatório")]
        public string Local { get; set; }
    }
}