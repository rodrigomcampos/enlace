﻿using Repositorio.DAL.Repositorios;
using System.ComponentModel.DataAnnotations;

namespace Repositorio.Aplicacao.Models.Request
{
    public class EditarItemRequestModel
    {
        [Required(ErrorMessage = "Id do arquivo é obrigatório")]
        public int IdArquivo { get; set; }

        [StringLength(200, MinimumLength = 1, ErrorMessage = "Título da biblioteca inválido")]
        public string Titulo { get; set; }

        public string Autor { get; set; }

        [StringLength(200, MinimumLength = 1, ErrorMessage = "Arquivo inválido")]
        public string Arquivo { get; set; }

        [Required(ErrorMessage = "Tipo é obrigatório")]
        public TipoBiblioteca Tipo { get; set; }

        public void Editar()
        {
            var biblioteca = new TB_BibliotecaRepositorio();
            var edit = biblioteca.FirstOrDefaultWhereCondition(t => t.IdBiblioteca == this.IdArquivo);

            edit.Titulo = this.Titulo;
            edit.Autor = this.Autor;
            edit.Arquivo = this.Arquivo;
            edit.CodigoTipo = (int)this.Tipo;

            biblioteca.Update(edit);
            biblioteca.SaveAllChanges();
        }
    }
}