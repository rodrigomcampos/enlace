﻿using System.ComponentModel.DataAnnotations;

namespace Repositorio.Aplicacao.Models.Request
{
    public class LoginRequestModel
    {
        [Required(ErrorMessage = "Login é obrigatório")]
        [RegularExpression(@"^\w+$", ErrorMessage = "Login inválido")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Senha é obrigatória")]
        [RegularExpression(@"^\w+$", ErrorMessage = "Senha inválida")]
        public string Senha { get; set; }
    }
}