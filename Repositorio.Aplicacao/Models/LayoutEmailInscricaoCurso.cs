﻿using Repositorio.Aplicacao.Models.Request;
using Repositorio.Comum.Global;
using Repositorio.DAL.Repositorios;
using Repositorio.Entidades.Enlace;
using System.IO;
using System.Linq;

namespace Repositorio.Aplicacao.Models
{
    public class LayoutEmailInscricaoCurso
    {
        public string CriarHtml(InscricaoCursoRequest request, TB_Liga_Curso_Turma turma)
        {
            var cidade = new CidadeRepositorio().GetWhere(x => x.Ibge == request.IbgeCidade)?.First();
            var estado = new EstadoRepositorio().GetWhere(x => x.IdEstado == cidade.IdEstado)?.First();

            var diretorioArquivo = Path.Combine(ClsGlobal.DiretorioBase, "LayoutMatricula.html");
            using (StreamReader str = new StreamReader(diretorioArquivo))
            {
                var layout = str.ReadToEnd();

                switch (request.Curso)
                {
                    case CursosEnum.ConstituicaoSujeitoIntervencoesTeoriaPraticaPsicanalitica:
                        layout = layout.Replace("#TITULOCURSO", "Teoria e Prática Psicanalítica: Constituição do Sujeito e Intervenções");
                        break;
                    case CursosEnum.NascimentoConstrucaoMente:
                        layout = layout.Replace("#TITULOCURSO", "Nascimento e Construção da Mente");
                        break;
                    case CursosEnum.SeminariosClinicos:
                        layout = layout.Replace("#TITULOCURSO", "Seminários Clínicos");
                        break;
                }

                layout = layout.Replace("#NOME", request.NomeAluno);
                layout = layout.Replace("#EMAIL", request.Email);
                layout = layout.Replace("#CPF", request.CPF);
                layout = layout.Replace("#INSTITUICAO", request.Instituicao);
                layout = layout.Replace("#ANOFORMACAO", request.AnoFormacao.ToString());
                layout = layout.Replace("#EMPRESA", request.NomeEmpresaTrabalha);
                layout = layout.Replace("#PROFISSAO", request.Profissao);
                layout = layout.Replace("#CEP", request.CEP);
                layout = layout.Replace("#LOGRADOURO", request.Logradouro);
                layout = layout.Replace("#BAIRRO", request.Bairro);
                layout = layout.Replace("#COMPLEMENTO", request.Complemento);
                layout = layout.Replace("#NUMERO", request.Numero.ToString());
                layout = layout.Replace("#ESTADO", estado?.Nome ?? string.Empty);
                layout = layout.Replace("#CIDADE", cidade?.Nome ?? string.Empty);
                layout = layout.Replace("#CONTATO", $"{request.Celular} | {request.Telefone}");
                layout = layout.Replace("#TURMA", $"{turma.DiaSemana}");
                layout = layout.Replace("#UNIDADE", $"{request.Local}");

                return layout;
            }
        }
    }
}