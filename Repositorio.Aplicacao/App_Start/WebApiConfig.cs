﻿using Repositorio.Aplicacao.Exceptions;
using Repositorio.Aplicacao.Filters;
using Repositorio.Aplicacao.Validate;
using System.Web.Http;

namespace Repositorio.Aplicacao
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "With_Controller-Action-Id",
                routeTemplate: "api/{controller}/{action}/{id}"
            );

            config.Routes.MapHttpRoute(
                name: "With_Controller-Action",
                routeTemplate: "api/{controller}/{action}"
            );

            config.Filters.Add(new ExceptionFilter());
            config.Filters.Add(new ValidateModelAttribute());
            config.Filters.Add(new AuthenticationAttribute());
        }
    }
}
