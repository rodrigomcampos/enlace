﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public class TB_Liga_Curso_Documentos_Necessarios
    {
        public int IdCursoDocumentosNecessarios { get; set; }
        public int xIdCurso { get; set; }
        public int xIdDocumentosNecessarios { get; set; }
        public bool Ativo { get; set; } = true;
        public DateTime Date { get; set; } = DateTime.Now;

        public virtual TB_Curso TB_Curso { get; set; }
        public virtual TB_Documentos_Necessarios TB_Documentos_Necessarios { get; set; }
    }

    public sealed class TB_Liga_Curso_Documentos_NecessariosConfiguration : EntityTypeConfiguration<TB_Liga_Curso_Documentos_Necessarios>
    {
        public TB_Liga_Curso_Documentos_NecessariosConfiguration()
        {
            ToTable("TB_Liga_Curso_Documentos_Necessarios");
            HasKey(t => t.IdCursoDocumentosNecessarios)
                .Property(t => t.IdCursoDocumentosNecessarios)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            HasRequired(t => t.TB_Curso)
                .WithMany(t => t.TB_Liga_Curso_Documentos_Necessarios)
                .HasForeignKey(t => t.xIdCurso)
                .WillCascadeOnDelete(false);

            HasRequired(t => t.TB_Documentos_Necessarios)
                .WithMany(t => t.TB_Liga_Curso_Documentos_Necessarios)
                .HasForeignKey(t => t.xIdDocumentosNecessarios)
                .WillCascadeOnDelete(false);
        }
    }
}
