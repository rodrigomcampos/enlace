﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public class TB_Liga_Curso_Inscricao
    {
        [Key]
        public int IdCursoInscricao { get; set; }

        [Required]
        public int xIdCurso { get; set; }

        [Required]
        [StringLength(maximumLength: 200)]
        public string NomeAluno { get; set; }

        [Required]
        [StringLength(maximumLength: 100)]
        public string Email { get; set; }

        [Required]
        [StringLength(maximumLength: 100)]
        public string Profissao { get; set; }

        [StringLength(maximumLength: 200)]
        public string Instituicao { get; set; }

        [Required]
        public int AnoFormacao { get; set; }

        [StringLength(maximumLength: 100)]
        public string NomeEmpresaTrabalha { get; set; }

        [Required]
        [StringLength(maximumLength: 11)]
        public string CPF { get; set; }

        [Required]
        [StringLength(maximumLength: 8)]
        public string CEP { get; set; }

        [Required]
        [StringLength(maximumLength: 100)]
        public string Logradouro { get; set; }

        [Required]
        public int Numero { get; set; }

        [StringLength(maximumLength: 100)]
        public string Complemento { get; set; }

        [Required]
        [StringLength(maximumLength: 100)]
        public string Bairro { get; set; }

        [Required]
        public long IbgeCidade { get; set; }

        [StringLength(maximumLength: 20)]
        public string Telefone { get; set; }

        [StringLength(maximumLength: 20)]
        public string Celular { get; set; }

        [StringLength(maximumLength: 100)]
        public string DiaSemana { get; set; }

        public DateTime DataInicio { get; set; }

        [StringLength(maximumLength: 100)]
        public string Horario { get; set; }

        [Required]
        [StringLength(maximumLength: 1000)]
        public string Unidade { get; set; }

        public virtual TB_Curso TB_Curso { get; set; }
    }

    public sealed class TB_Liga_Curso_InscricaoConfiguration : EntityTypeConfiguration<TB_Liga_Curso_Inscricao>
    {
        public TB_Liga_Curso_InscricaoConfiguration()
        {
            ToTable("TB_Liga_Curso_Inscricao");
            HasRequired(x => x.TB_Curso)
                .WithMany(x => x.TB_Liga_Curso_Inscricao)
                .HasForeignKey(x => x.xIdCurso)
                .WillCascadeOnDelete(false);
        }
    }
}