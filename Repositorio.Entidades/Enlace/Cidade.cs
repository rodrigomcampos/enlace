﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public class Cidade
    {
        [Key]
        public int IdCidade { get; set; }

        [StringLength(120)]
        public string Nome { get; set; }

        public int IdEstado { get; set; }

        public int Ibge { get; set; }

        public virtual Estado Estado { get; set; }

        public virtual ICollection<TB_Liga_Curso_Unidade> TB_Liga_Curso_Unidade { get; set; } = new HashSet<TB_Liga_Curso_Unidade>();
    }

    public sealed class CidadeConfiguration : EntityTypeConfiguration<Cidade>
    {
        public CidadeConfiguration()
        {
            ToTable("Cidade");
            HasRequired(x => x.Estado)
                .WithMany(x => x.Cidade)
                .HasForeignKey(x => x.IdEstado)
                .WillCascadeOnDelete(false);
        }
    }
}
