﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public partial class TB_Biblioteca
    {
        public int IdBiblioteca { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public string Arquivo { get; set; }

        [ForeignKey("TB_Tipo_Biblioteca")]
        public int CodigoTipo { get; set; }

        public bool Ativo { get; set; } = true;
        public DateTime Date { get; set; } = DateTime.Now;

        public virtual TB_Tipo_Biblioteca TB_Tipo_Biblioteca { get; set; }
    }

    public sealed class TB_BibliotecaConfiguration : EntityTypeConfiguration<TB_Biblioteca>
    {
        public TB_BibliotecaConfiguration()
        {
            ToTable("TB_Biblioteca");
            HasKey(x => x.IdBiblioteca)
                .Property(x => x.IdBiblioteca)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(x => x.Titulo).HasMaxLength(200).IsRequired();
            Property(x => x.Autor).HasMaxLength(200).IsRequired();
            Property(x => x.Arquivo).HasMaxLength(2000).IsOptional();
            Property(x => x.Ativo).IsRequired();
            Property(x => x.Date).IsRequired();

            HasRequired(x => x.TB_Tipo_Biblioteca)
                .WithRequiredPrincipal(x => x.TB_Biblioteca)
                .WillCascadeOnDelete(false);
        }
    }
}
