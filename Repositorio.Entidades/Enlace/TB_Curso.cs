namespace Repositorio.Entidades.Enlace
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class TB_Curso
    {
        [Key]
        public int IdCurso { get; set; }

        [Required]
        public int CodigoCurso { get; set; }

        [Required]
        [StringLength(100)]
        public string Nome { get; set; }

        [Required]
        public int CargaHoraria { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Sobre { get; set; }

        [Column(TypeName = "text")]
        public string Informacoes { get; set; }

        [Column(TypeName = "text")]
        public string Destinado { get; set; }        

        public decimal? ValorMatricula { get; set; }
        
        public decimal? ValorTotal { get; set; }

        public int? QuantidadeParcelas { get; set; }

        public decimal? ValorParcelas { get; set; }        

        [Column(TypeName = "text")]
        public string EnderecoRealizacao { get; set; }

        public string CaminhoImagem { get; set; }

        public int Ativo { get; set; } = 1;

        public DateTime Date { get; set; } = DateTime.Now;

        [StringLength(50)]
        public string Duracao { get; set; }

        public virtual ICollection<TB_Liga_Curso_Inscricao> TB_Liga_Curso_Inscricao { get; set; } = new HashSet<TB_Liga_Curso_Inscricao>();        

        public virtual ICollection<TB_Liga_Curso_Documentos_Necessarios> TB_Liga_Curso_Documentos_Necessarios { get; set; } = new HashSet<TB_Liga_Curso_Documentos_Necessarios>();

        public virtual ICollection<TB_Liga_Curso_Unidade> TB_Liga_Curso_Unidade { get; set; } = new HashSet<TB_Liga_Curso_Unidade>();
    }
}
