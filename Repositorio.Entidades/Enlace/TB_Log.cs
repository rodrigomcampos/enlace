using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public partial class TB_Log
    {
        public int IdLog { get; set; }

        public string Exception { get; set; }

        public string InnerException { get; set; }

        public string StackTrace { get; set; }

        public string Observacao { get; set; }

        public DateTime? Date { get; set; } = DateTime.Now;
    }

    public sealed class TB_Log_Configuration : EntityTypeConfiguration<TB_Log>
    {
        public TB_Log_Configuration()
        {
            ToTable("TB_Log");

            HasKey(x => x.IdLog)
                .Property(x => x.IdLog)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(x => x.Exception).IsOptional();
            Property(x => x.InnerException).IsOptional();
            Property(x => x.StackTrace).IsOptional();
            Property(x => x.Observacao).IsOptional();
            Property(x => x.Date).HasColumnType("datetime2").IsOptional();
        }
    }

}
