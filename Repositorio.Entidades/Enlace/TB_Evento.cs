﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public class TB_Evento
    {
        public int IdEvento { get; set; }
        public string NomeEvento { get; set; }
        public string TipoEvento { get; set; }
        public string UrlImagem { get; set; }
        public string UrlDocumento { get; set; }
        public string LinkFormulario { get; set; }
        public DateTime DataEvento { get; set; }
        public string Horario { get; set; }
        public string Local { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public bool Ativo { get; set; } = true;
    }

    public sealed class TB_EventoConfiguration : EntityTypeConfiguration<TB_Evento>
    {
        public TB_EventoConfiguration()
        {
            ToTable("TB_Evento");
            HasKey(x => x.IdEvento)
                .Property(x => x.IdEvento)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(x => x.NomeEvento).HasMaxLength(200).IsRequired();
            Property(x => x.TipoEvento).HasMaxLength(100).IsRequired();
            Property(x => x.UrlImagem).HasMaxLength(500).IsOptional();
            Property(x => x.UrlDocumento).HasMaxLength(500).IsOptional();
            Property(x => x.LinkFormulario).HasMaxLength(500).IsOptional();
            Property(x => x.DataEvento).HasColumnType("Date").IsOptional();
            Property(x => x.Horario).HasMaxLength(100).IsOptional();
            Property(x => x.Local).HasMaxLength(200).IsOptional();
        }
    }
}
