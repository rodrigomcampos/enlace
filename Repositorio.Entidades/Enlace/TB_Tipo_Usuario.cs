﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public class TB_Tipo_Usuario
    {
        public int IdTipoUsuario { get; set; }
        public int Codigo { get; set; }
        public string Descricao { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;

        public virtual ICollection<TB_Usuario> TB_Usuario { get; set; } = new HashSet<TB_Usuario>();
    }

    public sealed class TB_Tipo_UsuarioConfiguration : EntityTypeConfiguration<TB_Tipo_Usuario>
    {
        public TB_Tipo_UsuarioConfiguration()
        {
            ToTable("TB_Tipo_Usuario");
            HasKey(x => x.Codigo);
            Property(x => x.IdTipoUsuario)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(x => x.Descricao).HasMaxLength(50).IsRequired();
            Property(x => x.Date).IsRequired();
        }
    }
}
