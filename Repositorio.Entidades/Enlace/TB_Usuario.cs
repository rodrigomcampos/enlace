﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public class TB_Usuario
    {
        public int IdUsuario { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public int TipoUsuario { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;

        public virtual TB_Tipo_Usuario TB_Tipo_Usuario { get; set; }
    }

    public sealed class TB_UsuarioConfiguration : EntityTypeConfiguration<TB_Usuario>
    {
        public TB_UsuarioConfiguration()
        {
            ToTable("TB_Usuario");
            HasKey(x => x.IdUsuario)
                .Property(x => x.IdUsuario)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(x => x.Nome).HasMaxLength(200).IsRequired();
            Property(x => x.Login).HasMaxLength(200).IsRequired();
            Property(x => x.Senha).HasMaxLength(50).IsRequired();
            Property(x => x.TipoUsuario).IsRequired();
            Property(x => x.Date).IsRequired();

            HasRequired(x => x.TB_Tipo_Usuario)
                .WithMany(x => x.TB_Usuario)
                .HasForeignKey(x => x.TipoUsuario)
                .WillCascadeOnDelete(false);
        }
    }
}
