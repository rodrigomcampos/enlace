﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public class TB_Tipo_Biblioteca
    {
        public int IdTipoBiblioteca { get; set; }
        public int Codigo { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; } = true;
        public DateTime Date { get; set; } = DateTime.Now;

        public virtual TB_Biblioteca TB_Biblioteca { get; set; }
    }

    public sealed class TB_Tipo_BibliotecaConfiguration : EntityTypeConfiguration<TB_Tipo_Biblioteca>
    {
        public TB_Tipo_BibliotecaConfiguration()
        {
            ToTable("TB_Tipo_Biblioteca");
            HasKey(x => x.Codigo);

            Property(x => x.IdTipoBiblioteca)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(x => x.Descricao).HasMaxLength(50).IsRequired();
            Property(x => x.Ativo).IsRequired();
            Property(x => x.Date).IsRequired();
        }
    }
}
