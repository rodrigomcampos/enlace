﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public partial class TB_Liga_Curso_Unidade
    {
        public int IdLigaCursoUnidade { get; set; }
        
        public int CodigoCurso { get; set; }

        public int IdCidade { get; set; }

        public int? CargaHoraria { get; set; }

        public decimal? ValorMatricula { get; set; }

        public decimal? ValorTotal { get; set; }

        public int? QuantidadeParcelas { get; set; }

        public decimal? ValorParcelas { get; set; }
        
        public string EnderecoRealizacao { get; set; }
        public string Duracao { get; set; }

        public bool Ativo { get; set; } = true;

        public DateTime Date { get; set; } = DateTime.Now;

        public virtual Cidade Cidade { get; set; }
        public virtual TB_Curso TB_Curso { get; set; }

        public virtual ICollection<TB_Liga_Curso_Turma> TB_Liga_Curso_Turma { get; set; } = new HashSet<TB_Liga_Curso_Turma>();
    }

    public sealed class TB_Liga_Curso_Unidade_Configuration : EntityTypeConfiguration<TB_Liga_Curso_Unidade>
    {
        public TB_Liga_Curso_Unidade_Configuration()
        {
            ToTable("TB_Liga_Curso_Unidade");

            HasKey(x => x.IdLigaCursoUnidade)
                .Property(x => x.IdLigaCursoUnidade)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            Property(x => x.CodigoCurso).IsRequired();
            Property(x => x.IdCidade).IsRequired();
            Property(x => x.CargaHoraria).IsOptional();
            Property(x => x.ValorMatricula).IsOptional();
            Property(x => x.ValorTotal).IsOptional();
            Property(x => x.QuantidadeParcelas).IsOptional();
            Property(x => x.ValorParcelas).IsOptional();
            Property(x => x.EnderecoRealizacao).IsOptional();
            Property(x => x.Duracao).IsOptional();
            Property(x => x.Ativo).IsRequired();
            Property(x => x.Date).IsRequired();

            HasRequired(x => x.TB_Curso)
                .WithMany(x => x.TB_Liga_Curso_Unidade)
                .HasForeignKey(x => x.CodigoCurso)
                .WillCascadeOnDelete(true);

            HasRequired(x => x.Cidade)
              .WithMany(x => x.TB_Liga_Curso_Unidade)
              .HasForeignKey(x => x.IdCidade)
              .WillCascadeOnDelete(true);
        }
    }
}
