﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public class TB_Liga_Curso_Turma
    {
        public int IdCursoTurma { get; set; }
        
        public string DiaSemana { get; set; }

        public DateTime DataInicio { get; set; }

        public string Horario { get; set; }

        public int IdLigaCursoUnidade { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        public virtual TB_Liga_Curso_Unidade TB_Liga_Curso_Unidade { get; set; }
    }

    public sealed class TB_Liga_Curso_TurmaConfiguration : EntityTypeConfiguration<TB_Liga_Curso_Turma>
    {
        public TB_Liga_Curso_TurmaConfiguration()
        {
            ToTable("TB_Liga_Curso_Turma");
            HasKey(x => x.IdCursoTurma)
                .Property(x => x.IdCursoTurma)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            Property(x => x.DiaSemana).HasMaxLength(100).IsRequired();
            Property(x => x.DataInicio).IsRequired();
            Property(x => x.IdLigaCursoUnidade).IsRequired();
            Property(x => x.Horario).HasMaxLength(100).IsRequired();

            HasRequired(x => x.TB_Liga_Curso_Unidade)
                .WithMany(x => x.TB_Liga_Curso_Turma)
                .HasForeignKey(x => x.IdLigaCursoUnidade)
                .WillCascadeOnDelete(true);
        }
    }

}
