﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Repositorio.Entidades.Enlace
{

    public class Estado
    {
        [Key]
        public int IdEstado { get; set; }

        [Required]
        [StringLength(75)]
        public string Nome { get; set; }
        
        [StringLength(2)]
        public string Uf { get; set; }

        public int Ibge { get; set; }

        public int UF_SL { get; set; }

        [StringLength(50)]
        public string UF_DDD { get; set; }

        public virtual ICollection<Cidade> Cidade { get; set; } = new HashSet<Cidade>();
    }
}
