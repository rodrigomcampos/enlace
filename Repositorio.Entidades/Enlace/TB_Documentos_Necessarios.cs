﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Repositorio.Entidades.Enlace
{
    public class TB_Documentos_Necessarios
    {
        public int IdDocumentosNecessarios { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; } = true;
        public DateTime Date { get; set; } = DateTime.Now;

        public virtual ICollection<TB_Liga_Curso_Documentos_Necessarios> TB_Liga_Curso_Documentos_Necessarios { get; set; } = new HashSet<TB_Liga_Curso_Documentos_Necessarios>();
    }

    public sealed class TB_Documentos_NecessariosConfiguration : EntityTypeConfiguration<TB_Documentos_Necessarios>
    {
        public TB_Documentos_NecessariosConfiguration()
        {
            ToTable("TB_Documentos_Necessarios");
            HasKey(t => t.IdDocumentosNecessarios)
                .Property(t => t.IdDocumentosNecessarios)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(t => t.Descricao).HasMaxLength(100).IsRequired();
        }
    }
}
