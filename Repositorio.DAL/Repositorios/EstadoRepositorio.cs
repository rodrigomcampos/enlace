﻿using Repositorio.DAL.Contexto;
using System.Collections.Generic;
using System.Linq;

namespace Repositorio.DAL.Repositorios
{
    public class EstadoRepositorio : Base.Repositorio<Entidades.Enlace.Estado, EnlaceContexto>
    {
        public IEnumerable<Comum.Models.Estado> GetEstados()
        {
            return GetAll().Select(x => new Comum.Models.Estado()
            {
                IdEstado = x.IdEstado,
                Nome = x.Nome,
                Ibge = x.Ibge
            });
        }
    }
}
