﻿using Repositorio.DAL.Contexto;
using Repositorio.Entidades.Enlace;
using System.Linq;

namespace Repositorio.DAL.Repositorios
{
    public class TB_UsuarioRepositorio : Base.Repositorio<TB_Usuario, EnlaceContexto>
    {
        public TB_Usuario RealizarLogin(string login, string senha)
        {
            var usuario = GetAll().Where(x => x.Login == login && x.Senha == senha)
                .FirstOrDefault();

            return usuario;
        }
    }
}
