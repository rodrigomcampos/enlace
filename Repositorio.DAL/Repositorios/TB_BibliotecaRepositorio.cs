﻿using Repositorio.DAL.Contexto;
using Repositorio.Entidades.Enlace;
using System.Collections.Generic;
using System.Linq;

namespace Repositorio.DAL.Repositorios
{
    public class TB_BibliotecaRepositorio : Base.Repositorio<TB_Biblioteca, EnlaceContexto>
    {
        public IEnumerable<object> GetArquivos()
        {
            return
            (from arq in GetAll()
             join liga in ctx.Set<TB_Tipo_Biblioteca>() on arq.CodigoTipo equals liga.Codigo
             select new
             {
                 IdArquivo = arq.IdBiblioteca,
                 Titulo = arq.Titulo,
                 Autor = arq.Autor,
                 Arquivo = arq.Arquivo,
                 Tipo = liga.Descricao,
                 Data = arq.Date,
                 Ativo = arq.Ativo
             })
             .ToList();
        }

        public IEnumerable<object> GetArquivosClient()
        {
            return
            (from arq in GetAll()
             join liga in ctx.Set<TB_Tipo_Biblioteca>() on arq.CodigoTipo equals liga.Codigo
             where arq.Ativo == true
             select new
             {
                 Titulo = arq.Titulo,
                 Autor = arq.Autor,
                 Arquivo = arq.Arquivo,
                 Tipo = liga.Descricao
             }).ToList();
        }

        public IEnumerable<object> GetArquivosClient(int codigoTipo)
        {
            return
            (from arq in GetAll()
             join liga in ctx.Set<TB_Tipo_Biblioteca>() on arq.CodigoTipo equals liga.Codigo
             where liga.Codigo == codigoTipo
             && arq.Ativo == true
             select new
             {
                 Titulo = arq.Titulo,
                 Autor = arq.Autor,
                 Arquivo = arq.Arquivo,
                 Tipo = liga.Descricao
             })
             .ToList();
        }
    }
}
