﻿using Repositorio.DAL.Contexto;
using Repositorio.DAL.Repositorios.Base;
using Repositorio.Entidades.Enlace;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repositorio.DAL.Repositorios
{
    public class TB_CursoRepositorio : Base.Repositorio<TB_Curso, EnlaceContexto>
    {
        public IEnumerable<object> GetCursos()
        {
            return (from curso in GetAll()
                    select new
                    {
                        IdCurso = curso.IdCurso,
                        CodigoCurso = curso.CodigoCurso,
                        Nome = curso.Nome,
                        DataCadastro = curso.Date
                    });
        }

        public object GetCursoAdm(int idCurso)
        {
            return GetCurso($"@IdCurso = {idCurso}");
        }

        public object GetCurso(int codigoCurso)
        {
            return GetCurso($"@CodigoCurso = {codigoCurso}");
        }

        private object GetCurso(string parametroSP)
        {
            using (ctx)
            {
                var cursos = ctx
                    .DynamicListFromSql($"EXEC SP_GetCurso {parametroSP}", new Dictionary<string, object>())
                    .ToList();

                if (cursos.Count == 0)
                    throw new Exception("Curso não encontrado");

                return cursos
                    .GroupBy(grp => grp.IdCurso)
                    .Select(grp =>
                    {
                        var listaDados = grp.ToList();
                        var curso = listaDados.First();

                        object documentos = null;

                        if (listaDados.Any(t => !string.IsNullOrWhiteSpace(t.IdDocumentos.ToString())))
                            documentos = listaDados
                                .GroupBy(grpDoc => grpDoc.IdDocumentos)
                                .Select(grpDoc =>
                                {

                                    var doc = grpDoc.ToList().First();

                                    return new
                                    {
                                        Id = doc.IdDocumentos,
                                        Descricao = doc.DescricaoDocumento
                                    };
                                });

                        object unidades = null;

                        if (listaDados.Any(t => !string.IsNullOrWhiteSpace(t.IdLigaCursoUnidade.ToString())))
                            unidades = listaDados
                                .GroupBy(grpUnidade => grpUnidade.IdLigaCursoUnidade)
                                .Select(grpUnidade =>
                                {
                                    var listaUnidade = grpUnidade.ToList();
                                    var unidade = listaUnidade.First();

                                    object turmas = null;

                                    if (listaUnidade.Any(t => !string.IsNullOrWhiteSpace(t.IdTurma.ToString())))
                                        turmas = listaUnidade
                                            .GroupBy(grpTurma => grpTurma.IdTurma)
                                            .Select(grpTurma =>
                                            {
                                                var turma = grpTurma.ToList().First();
                                                return new
                                                {
                                                    DataInicio = turma.DataInicio,
                                                    DiaSemana = turma.DiaSemana,
                                                    Horario = turma.Horario,
                                                    IdTurma = turma.IdTurma
                                                };
                                            });

                                    return new
                                    {
                                        Id = unidade.IdLigaCursoUnidade,
                                        CargaHoraria = unidade.CargaHoraria,
                                        ValorMatricula = string.Format("{0:n2}", unidade.ValorMatricula),
                                        ValorTotal = string.Format("{0:n2}", unidade.ValorTotal),
                                        QuantidadeParcelas = unidade.QuantidadeParcelas,
                                        ValorParcelas = string.Format("{0:n2}", unidade.ValorParcelas),
                                        EnderecoRealizacao = unidade.EnderecoRealizacao,
                                        Duracao = unidade.Duracao,
                                        Cidade = new
                                        {
                                            Id = unidade.CidadeId,
                                            Ibge = unidade.CidadeIbge,
                                            Nome = unidade.CidadeNome,
                                            Estado = new
                                            {
                                                Id = unidade.EstadoId,
                                                Ibge = unidade.EstadoIbge,
                                                Uf = unidade.EstadoUf,
                                                Nome = unidade.EstadoNome
                                            }
                                        },
                                        Turmas = turmas
                                    };
                                });

                        return new
                        {
                            IdCurso = curso.IdCurso,
                            CodigoCurso = curso.CodigoCurso,
                            Nome = curso.Nome,
                            Sobre = curso.Sobre,
                            Informacoes = curso.Informacoes,
                            Destinado = curso.Destinado,
                            Ativo = curso.Ativo,
                            CaminhoImagem = curso.CaminhoImagem,
                            DocumentosNecessarios = documentos,
                            Unidades = unidades
                        };
                    })
                    .First();
            }
        }
    }
}
