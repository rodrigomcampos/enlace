using Repositorio.Entidades.Enlace;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;

namespace Repositorio.DAL.Contexto
{
    public partial class EnlaceContexto : DbContext
    {
        public EnlaceContexto()
            : base("name=EnlaceContexto")
        {
            Database.SetInitializer<EnlaceContexto>(null);
            this.Database.Initialize(true);
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;

#if DEBUG
            Database.Log = sql => Debug.WriteLine(sql);
#endif
        }

        public virtual DbSet<TB_Curso> TB_Cursos { get; set; }
        public virtual DbSet<TB_Log> TB_Log { get; set; }
        public virtual DbSet<TB_Liga_Curso_Inscricao> TB_Liga_Curso_Inscricao { get; set; }
        public virtual DbSet<TB_Liga_Curso_Turma> TB_Liga_Curso_Turma { get; set; }
        public virtual DbSet<Estado> Estado { get; set; }
        public virtual DbSet<Cidade> Cidade { get; set; }
        public virtual DbSet<TB_Tipo_Usuario> TB_Tipo_Usuario { get; set; }
        public virtual DbSet<TB_Usuario> TB_Usuario { get; set; }
        public virtual DbSet<TB_Evento> TB_Evento { get; set; }
        public virtual DbSet<TB_Documentos_Necessarios> TB_Documentos_Necessarios { get; set; }
        public virtual DbSet<TB_Liga_Curso_Documentos_Necessarios> TB_Liga_Curso_Documentos_Necessarios { get; set; }
        public virtual DbSet<TB_Biblioteca> TB_Biblioteca { get; set; }
        public virtual DbSet<TB_Tipo_Biblioteca> TB_Tipo_Biblioteca { get; set; }
        public virtual DbSet<TB_Liga_Curso_Unidade> TB_Liga_Curso_Unidade { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<TB_Curso>()
                .Property(e => e.Nome)
                .IsUnicode(false);

            modelBuilder.Entity<TB_Curso>()
                .Property(e => e.Destinado)
                .IsUnicode(false);

            modelBuilder.Entity<TB_Curso>()
                .Property(e => e.ValorParcelas)
                .HasPrecision(15, 2);

            modelBuilder.Entity<TB_Curso>()
                .Property(e => e.ValorTotal)
                .HasPrecision(15, 2);

            modelBuilder.Configurations.Add(new TB_Log_Configuration());
            modelBuilder.Configurations.Add(new TB_Liga_Curso_InscricaoConfiguration());
            modelBuilder.Configurations.Add(new CidadeConfiguration());
            modelBuilder.Configurations.Add(new TB_Tipo_UsuarioConfiguration());
            modelBuilder.Configurations.Add(new TB_UsuarioConfiguration());
            modelBuilder.Configurations.Add(new TB_Liga_Curso_TurmaConfiguration());
            modelBuilder.Configurations.Add(new TB_EventoConfiguration());
            modelBuilder.Configurations.Add(new TB_Documentos_NecessariosConfiguration());
            modelBuilder.Configurations.Add(new TB_Liga_Curso_Documentos_NecessariosConfiguration());
            modelBuilder.Configurations.Add(new TB_Tipo_BibliotecaConfiguration());
            modelBuilder.Configurations.Add(new TB_BibliotecaConfiguration());
            modelBuilder.Configurations.Add(new TB_Liga_Curso_Unidade_Configuration());
        }
    }
}
