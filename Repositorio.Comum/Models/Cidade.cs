﻿namespace Repositorio.Comum.Models
{
    public class Cidade
    {
        public int IdCidade { get; set; }        
        public string Nome { get; set; }
        public int IdEstado { get; set; }
        public int Ibge { get; set; }
    }
}
