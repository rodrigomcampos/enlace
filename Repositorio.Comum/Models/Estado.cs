﻿namespace Repositorio.Comum.Models
{
    public sealed class Estado
    {
        public int IdEstado { get; set; }
        public string Nome{ get; set; }
        public int Ibge { get; set; }
    }
}
